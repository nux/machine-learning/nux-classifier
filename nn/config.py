import os
import argparse
from nn.operations import str2bool
from tensorflow import config

try:
    from .directories import homedir
except ImportError:
    homedir = os.curdir


def get_args():
    desc = "TF2 implementation of a Self-Supervised Class-Attention Cross-Covariance Visual Transformer"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("--model_name", nargs="?", type=str, default="xcit_tst",
                        help="The model name. One of: cait_xxs24, cait_xs36, cait_s24, cait_m24, "
                             "cait_s48, cait_s36, xcit_s12, xcit_s24,, xcit_m24",
                        choices=["xcit_tst", "cait_xxs24", "cait_xs36", "cait_s24",
                                 "cait_m24", "cait_s48", "cait_s36",
                                 "xcit_s12", "xcit_s24", "xcit_m24"])

    parser.add_argument("--dataset", nargs="?", type=str, default="stl10",
                        help="String that is passed to Squirrel. "
                             "See sq.list_builders() for avail. datasets.")

    parser.add_argument("--split_list", type=list, nargs="+",
                        default=["test", "unlabelled"],
                        # default=["train[:10%]", "train[10%:]"],
                        help="The 'split' argument for squirrel. "
                             "A python 'list' is expected. "
                             "Example: ['test', 'train'] can be passed as: "
                             "python main.py --split_list test train")

    parser.add_argument("--epochs", nargs="?", type=int, default=int(300),
                        help="The number of epochs to run")

    parser.add_argument("--save_every_n_epoch", nargs="?", type=int, default=25,
                        help="Save model weights every 'n' epochs")

    parser.add_argument("--test_every_n_epoch", nargs="?", type=int, default=10,
                        help="Evaluate model every 'n' epochs")

    parser.add_argument("--img_size", nargs="?", type=int, default=128,
                        help="Image size to which the input will be resized")

    parser.add_argument("--img_channel", nargs="?", type=int, default=3,
                        help="Image channel to which the input will be expanded/reduced")

    parser.add_argument("--log_steps", nargs="?", type=int, default=100,
                        help="Write out logs every 'n' steps")

    parser.add_argument("--batch_size", nargs="?", type=int, default=int(32),
                        help="The size of batch per gpu.")

    parser.add_argument("--clr_temperature", nargs="?", type=float, default=float(0.5),
                        help="The temperature parameter for the contrastive task during pre-training")

    parser.add_argument("--optimizer", nargs="?", type=str, default="adamw",
                        help="The used optimizer. Choices are either 'sgd', 'adam', 'adamw'",
                        choices=["sgd", "adam", "adamw"])

    parser.add_argument("--lr", nargs="?", type=float, default=.0,
                        help="Learning rate. Leave zero for batch-size dependent estimation of the lr'")

    parser.add_argument("--l2", nargs="?", type=float, default=.05,
                        help="L2 regularization term. Sometimes called 'weight decay'")

    parser.add_argument("--deep_root", nargs="?", type=str2bool, default="true",
                        help="Use a deep root instead of the standard ViT patchify stem",
                        choices=[True, False])

    parser.add_argument("--sam", nargs="?", type=str2bool, default="true",
                        help="Use sharpness-aware minimization",
                        choices=[True, False])

    parser.add_argument("--sam_rho", type=float, default=0.15,
                        help="Rho for the sharpness-aware minimization")

    parser.add_argument("--use_fourier_embedding", nargs="?", type=str2bool, default="true",
                        help="Use a Fourier Embedding as in the original Vaswani Paper",
                        choices=[True, False])

    parser.add_argument("--use_mixup", nargs="?", type=str2bool, default="true",
                        help="Use Mixup augmentation",
                        choices=[True, False])

    parser.add_argument("--mixup_alpha", type=float, default=0.5,
                        help="Alpha for the Mixup augmentation")

    parser.add_argument("--patch_size", nargs="?", type=int, default=int(16),
                        help="The input patch size",
                        choices=[int(16), int(8)])

    parser.add_argument("--base_model_dir", nargs="?", type=str,
                        default="",
                        help="This argument is only used in the fine-tune routine,"
                             "and provides a path to a base model to use. This"
                             "base model has to be pre-trained.")

    parser.add_argument("--base_model_epoch", nargs="?", type=str, default="",
                        help="This argument is only used in the fine-tune routine,"
                             "The epoch to load. Leave this empty for the latest model.")

    parser.add_argument("--fp16", nargs="?", type=str2bool, default="false",
                        help="Use mixed precision or not. Set to True if your NVidia GPU "
                             "has compute capability > 7.0",
                        choices=[True, False])

    parser.add_argument("--gpus", nargs="?", type=str, default="all",
                        help="Number of GPUs to be used. Can be index-based '0,1,2' or "
                             "'all' for all available")

    parser.add_argument("--work_dir", nargs="?", type=str, default=homedir,
                        help="The work directory. "
                             "Missing directories will be created")

    return check_args(parser.parse_args())


def check_args(args):
    """
    Checking arguments
    """
    try:
        assert args.epochs >= 1
    except AssertionError:
        args.logger.error("\t[!] Number of epochs must be larger than or equal to one")

    args.split_list = ["".join(x) for x in args.split_list]

    if sum([x.find("GPU:") for x in args.gpus]) < 0:  # Entries like "GPU:0" can be kept
        if "all" in args.gpus:
            gpus = config.list_physical_devices("GPU")
            args.gpus = [x.name[x.name.find(":") + 1:] for x in gpus]
        else:
            gpus = list(args.gpus)
            for ii in gpus:  # Get rid of every non integer in the string
                try:
                    int(ii)
                except ValueError:
                    gpus = list(filter(ii.__ne__, gpus))
            gpus.sort()
            args.gpus = ["GPU:{}".format(x) for x in gpus]

    if len(args.gpus) < 1:
        args.logger.error("\t[!] Please provide a valid argument for the gpu selector")

    return args
