import sys
import string
import numpy as np
import tensorflow as tf
from abc import ABC

import nn

tfk = tf.keras
tfkl = tfk.layers
_CHR_IDX = string.ascii_lowercase


class ViT(tfk.Model, ABC):
    """
    This implements self-supervised CaiT Visual Transformer Network.

    Dosovitskiy, A. et al.
    An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale. (2020).

    We use LayerScale and the class-attention layers (CaiT) proposed in:
    Touvron, H., Cord, M., Sablayrolles, A., Synnaeve, G. & Jégou, H.
    Going deeper with Image Transformers. (2021).

    For self-supervised pretraining we use the SiT framework and the
    the uncertainty weighting approach (multi-task learning loss):
    Atito, S., Awais, M. & Kittler, J.
    SiT: Self-supervised vIsion Transformer. (2021)

    For more details on the uncertainy weighting approach:
    Kendall, A.; Gal, Y. & Cipolla, R.
    Multi-task learning using uncertainty to weigh losses for scene geometry and semantics
    Proceedings of the IEEE conference on computer vision and pattern recognition (2018)

    """

    def __init__(self, input_shape, num_classes,
                 layers, num_cait_layers, hidden_size, mlp_size, heads, pretrain,
                 l2, dropout, patch_size, epsilon, drop_rate, xcit=False,
                 deep_root=True, use_fourier_embedding=True,
                 **kwargs):
        super(ViT, self).__init__(**kwargs)
        self.shape = input_shape
        self.pretrain = pretrain
        self.l2 = l2
        self.num_classes = num_classes
        self.num_layers = layers
        self.num_cait_layers = num_cait_layers
        self.hidden_size = hidden_size
        self.mlp_size = mlp_size
        self.heads = heads
        self.dropout = dropout
        self.epsilon = epsilon
        self.drop_rate = drop_rate
        self.deep_root = deep_root
        self.use_fourier_embedding = use_fourier_embedding
        if self.deep_root:
            self.num_layers -= 1

        self.patch_size = patch_size
        self.num_patches = np.ceil(self.shape[1] / self.patch_size).astype(int) ** 2

        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(self.l2)}

        if self.pretrain:
            base_mlp_head = tf.keras.Sequential()
        else:
            base_mlp_head = tf.keras.Sequential(
                [
                    tfkl.LayerNormalization(epsilon=1e-6),
                    tfkl.Dense(self.mlp_size, activation=tf.nn.gelu, **shared_args),
                    tfkl.Dropout(self.dropout),
                ]
            )
        if self.deep_root:
            self.stem = ConvolutionalRootEmbedding(self.l2, self.hidden_size, self.patch_size)
        else:
            self.stem = ViTPatches(self.patch_size, self.hidden_size, self.l2)
        self.patch_reshape = tfkl.Reshape([self.num_patches, self.hidden_size])

        if self.use_fourier_embedding:
            self.PatchEmbeddings = PositionalEncodingFourier(embed_dim=self.hidden_size,
                                                             dropout=max(self.dropout, self.drop_rate))
        else:
            self.PatchEmbeddings = PatchEncoder(num_patches=self.num_patches,
                                                projection_dim=self.hidden_size,
                                                l2=self.l2, dropout=max(self.dropout, self.drop_rate))

        self.TokenEmbeddings = TokenEncoder(projection_dim=self.hidden_size,
                                            num_patches=self.num_patches)

        self.transformer_layers = []
        transformer_args = {"embed_dim": self.hidden_size, "num_heads": self.heads, "epsilon": self.epsilon,
                            "mlp_size": self.mlp_size, "l2": self.l2, "dropout": self.dropout,
                            "drop_rate": self.drop_rate}

        if xcit:
            block = XCABlock
        else:
            block = TransformerBlock
        for _ in range(self.num_layers):
            self.transformer_layers.append(block(**transformer_args))

        self.cait_layers = []
        for _ in range(self.num_cait_layers):
            self.cait_layers.append(TransformerBlock(**transformer_args,
                                                     class_attention=True))

        # This linear activation is needed in case of mixed precision training,
        # it then acts as a casting layer
        self.out = tfkl.Activation("linear", dtype="float32", name="Casting")

        self.rot_head = base_mlp_head
        self.contrastive_head = tfk.models.clone_model(base_mlp_head)

        if self.pretrain:
            reshape_patch_size = np.sqrt(self.num_patches).astype(int)
            self.reshape_x_rec = tfkl.Reshape([reshape_patch_size, reshape_patch_size, self.hidden_size])

            self.rot_w = self.add_weight("rot_w", shape=[1],
                                         initializer="zeros",
                                         trainable=True,
                                         dtype=self.variable_dtype)
            self.contrastive_w = self.add_weight("contrastive_w", shape=[1],
                                                 initializer="zeros",
                                                 trainable=True,
                                                 dtype=self.variable_dtype)
            self.recons_w = self.add_weight("reconstruction_w", shape=[1],
                                            initializer="zeros",
                                            trainable=True,
                                            dtype=self.variable_dtype)

            self.backprojection = tfkl.Conv2DTranspose(filters=self.shape[-1],
                                                       kernel_size=(self.patch_size, self.patch_size),
                                                       strides=(self.patch_size, self.patch_size),
                                                       padding="valid", **shared_args)

            self.rot_head.add(tfkl.Dense(4, **shared_args))
            self.contrastive_head.add(tfkl.Dense(512, **shared_args))

        else:
            self.rot_head.add(tfkl.Dense(self.num_classes, **shared_args))
            self.contrastive_head.add(tfkl.Dense(self.num_classes, **shared_args))

    def call(self, inputs, training=None, **kwargs):
        # Get patches
        patches = self.stem(inputs)
        patch_size = tf.shape(patches)[1]

        patches = self.patch_reshape(patches)
        x = self.PatchEmbeddings(patches, training=training, size=patch_size)
        for transformer_layer in self.transformer_layers:
            x = transformer_layer(x, training=training)  # [bs, num_patches + 2, hidden_size]

        x_cait, x = self.TokenEmbeddings(x)
        for cait_layer in self.cait_layers:
            x_cait = cait_layer((x_cait, x), training=training)  # [bs, 2, hidden_size]

        x_rot = self.rot_head(x_cait[:, 0], training=training)  # [bs, 4 or num_classes]
        x_contrastive = self.contrastive_head(x_cait[:, 1], training=training)  # [bs, 512 or num_classes]

        if self.pretrain:
            # linearly project back to image space
            # [bs, sqrt(patch_size), sqrt(patch_size), num_patches]
            x_rec = self.reshape_x_rec(x)
            x_rec = self.backprojection(x_rec, training=training)  # [bs, self.shape[1], self.shape[2], self.shape[3]]
            out_vals = [x_rot, x_contrastive, x_rec, self.rot_w, self.contrastive_w, self.recons_w]
            return [self.out(x) for x in out_vals]

        out_vals = [x_rot, x_contrastive]
        return [self.out(x) for x in out_vals]


class PatchEncoder(tfkl.Layer):
    def __init__(self, num_patches, projection_dim, l2, dropout):
        super(PatchEncoder, self).__init__()
        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(l2)}

        self.num_patches = num_patches
        self.projection_dim = projection_dim
        self.projection = tfkl.Dense(units=projection_dim, **shared_args)

        self.position_embedding = self.add_weight("pos_emb", shape=[1, self.num_patches, self.projection_dim],
                                                  initializer=shared_args["kernel_initializer"],
                                                  trainable=True, dtype=self.variable_dtype)

        self.drop = tfkl.Dropout(dropout)

    def call(self, inputs, training=None, **kwargs):
        encoded = self.projection(inputs, training=training) + self.position_embedding
        return self.drop(encoded, training=training)


class PositionalEncodingFourier(tfkl.Layer):
    """
    Positional encoding relying on a fourier kernel matching the one used in the
    "Attention is all of Need" paper. The implementation builds on DeTR code
    https://github.com/facebookresearch/detr/blob/master/models/position_encoding.py

    This Tensorflow implementation is based on the XCIT Pytorch Implementation:
    https://github.com/facebookresearch/xcit/blob/master/xcit.py
    """

    def __init__(self, dropout, hidden_dim=32, embed_dim=768, temperature=10000):
        super().__init__()
        self.projection = tfkl.Conv2D(filters=embed_dim,
                                      kernel_size=(1, 1),
                                      strides=(1, 1),
                                      name="TokenProjection")
        self.drop = tfkl.Dropout(dropout)
        self.scale = 2 * np.pi
        self.temperature = temperature
        self.hidden_dim = hidden_dim
        self.dim = embed_dim

    def call(self, inputs, training=None, **kwargs):
        inputs_shape = tf.shape(inputs)
        not_mask = tf.ones((inputs_shape[0], kwargs["size"], kwargs["size"]))

        y_embed = tf.cast(tf.math.cumsum(not_mask, axis=1), tf.float32)
        x_embed = tf.cast(tf.math.cumsum(not_mask, axis=2), tf.float32)
        eps = 1e-6
        y_embed = y_embed / (y_embed[:, -1:, :] + eps) * self.scale
        x_embed = x_embed / (x_embed[:, :, -1:] + eps) * self.scale

        dim_t = tf.range(0, self.hidden_dim, dtype=tf.float32)
        dim_t = self.temperature ** (2 * (dim_t // 2) / self.hidden_dim)

        pos_x = tf.expand_dims(x_embed, -1) / dim_t
        pos_y = tf.expand_dims(y_embed, -1) / dim_t

        pos_x_shape = tf.shape(pos_x)
        pos_x = tf.reshape(tf.stack((tf.math.sin(pos_x[:, :, :, 0::2]),
                                     tf.math.cos(pos_x[:, :, :, 1::2])), axis=4),
                           [pos_x_shape[0], pos_x_shape[1], pos_x_shape[2], int(self.hidden_dim)])

        pos_y = tf.reshape(tf.stack((tf.math.sin(pos_y[:, :, :, 0::2]),
                                     tf.math.cos(pos_y[:, :, :, 1::2])), axis=4),
                           [pos_x_shape[0], pos_x_shape[1], pos_x_shape[2], int(self.hidden_dim)])

        pos = tf.concat((pos_y, pos_x), axis=3)
        encoded = tf.reshape(self.projection(pos, training=training),
                             (inputs_shape[0], -1, inputs_shape[-1]))
        return self.drop(inputs + encoded, training=training)


class ViTPatches(tfkl.Layer):
    def __init__(self, patch_size, embed_dim, l2):
        super(ViTPatches, self).__init__()
        self.patch_size = patch_size

        self.final_proj = tfkl.Conv2D(filters=embed_dim,
                                      kernel_size=(1, 1),
                                      strides=(1, 1),
                                      name="FinalDeepRoot",
                                      padding="VALID",
                                      activity_regularizer=tfk.regularizers.L2(l2))

    def call(self, inputs, training=None, **kwargs):
        inputs = tf.image.extract_patches(
            images=inputs,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        return self.final_proj(inputs, training=training)


class ConvolutionalRootEmbedding(tfkl.Layer):
    def __init__(self, l2, embed_dim, patch_size):
        """
        Implementation of:
        Xiao, T. et al. Early Convolutions Help Transformers See Better. (2021).

        :param l2: The weight decay
        :param embed_dim: patch dimensionality
        :param patch_size: The patch size ... Either 16 or 8
        """
        super(ConvolutionalRootEmbedding, self).__init__()
        shared_args = {"use_bias": False,
                       "kernel_initializer": tf.keras.initializers.GlorotUniform(),
                       "activity_regularizer": tfk.regularizers.L2(l2),
                       "kernel_size": (3, 3),
                       "padding": "VALID"}

        filter_sizes = [embed_dim // 8, embed_dim // 4, embed_dim // 2, embed_dim, embed_dim]
        stride_sizes = [2, 2, 2, 2, 1]

        self.root_convs = [tfkl.Conv2D(filters=c, strides=(s, s), **shared_args) for c, s in
                           zip(filter_sizes if patch_size == 16 else filter_sizes[1:],
                               stride_sizes if patch_size == 16 else stride_sizes[1:])]
        self.root_bns = [tfkl.experimental.SyncBatchNormalization() for _ in range(len(filter_sizes))]

        self.final_proj = tfkl.Conv2D(filters=embed_dim,
                                      kernel_size=(1, 1),
                                      strides=(1, 1),
                                      name="FinalDeepRoot",
                                      padding="VALID",
                                      activity_regularizer=tfk.regularizers.L2(l2))

    def call(self, inputs, training=None, **kwargs):
        for rc, rb in zip(self.root_convs, self.root_bns):
            inputs = tf.pad(inputs,
                            [[0, 0], [1, 1], [1, 1], [0, 0]],
                            "REFLECT")
            inputs = rc(inputs, training=training)
            inputs = rb(inputs, training=training)
            inputs = tf.nn.gelu(inputs)
        return self.final_proj(inputs, training=training)


class TokenEncoder(tfkl.Layer):
    def __init__(self, projection_dim, num_patches):
        super(TokenEncoder, self).__init__()
        self.num_patches = num_patches
        self.projection_dim = projection_dim
        self.contrastive_embedding = self.add_weight("con_emb", shape=[1, 1, self.projection_dim],
                                                     initializer=tfk.initializers.TruncatedNormal(stddev=0.02),
                                                     trainable=True, dtype=self.variable_dtype)
        self.rotation_embedding = self.add_weight("rot_emb", shape=[1, 1, self.projection_dim],
                                                  initializer=tfk.initializers.TruncatedNormal(stddev=0.02),
                                                  trainable=True, dtype=self.variable_dtype)

        self.concats = tfkl.Concatenate(axis=1)

    def call(self, inputs, training=None, **kwargs):
        input_shape = tf.shape(inputs)
        batch_size = input_shape[0]

        contrastive_embedding = tf.broadcast_to(
            self.contrastive_embedding, [batch_size, 1, self.projection_dim]
        )
        rotation_embedding = tf.broadcast_to(
            self.rotation_embedding, [batch_size, 1, self.projection_dim]
        )

        embedding_conc = self.concats([rotation_embedding, contrastive_embedding])
        return embedding_conc, inputs


class MultiHeadSelfAttention(tf.keras.layers.Layer):
    def __init__(self, embed_dim=768, num_heads=12, l2=None, dropout=None, class_attention=False):
        super(MultiHeadSelfAttention, self).__init__()
        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(l2)}

        self.embed_dim = embed_dim
        self.num_heads = num_heads
        self.class_attention = class_attention
        self.l2 = l2

        if not self.class_attention:
            # Talking Heads: arXiv:2003.02436 [cs.LG]
            # This is from the official TalkingHeadsAttention Implementation
            # https://github.com/tensorflow/models/blob/master/official/nlp/modeling/layers/talking_heads_attention.py
            num_batch_dims, attn_scores_rank = 1, 4
            scores_notation = _CHR_IDX[:attn_scores_rank]
            projection_notation = scores_notation[num_batch_dims] + (
                _CHR_IDX[attn_scores_rank])
            projected_scores_notation = scores_notation[:num_batch_dims] + (
                    _CHR_IDX[attn_scores_rank] + scores_notation[num_batch_dims + 1:])
            self._talking_heads_equation = "%s,%s->%s" % (
                scores_notation, projection_notation, projected_scores_notation)

            self._pre_softmax_weight = self.add_weight(
                "pre_softmax_weight",
                shape=(self.num_heads, self.num_heads),
                initializer=shared_args["kernel_initializer"],
                # regularizer=shared_args["activity_regularizer"],
                trainable=True,
                dtype=self.variable_dtype)
            self._post_softmax_weight = self.add_weight(
                "post_softmax_weight",
                shape=(self.num_heads, self.num_heads),
                initializer=shared_args["kernel_initializer"],
                # regularizer=shared_args["activity_regularizer"],
                trainable=True,
                dtype=self.variable_dtype)

        if embed_dim % num_heads != 0:
            raise ValueError(
                f"embedding dimension = {embed_dim} should be divisible by number of heads = {num_heads}"
            )
        self.projection_dim = embed_dim // num_heads
        self.query_dense = tfkl.Dense(embed_dim, **shared_args)
        self.key_dense = tfkl.Dense(embed_dim, **shared_args)
        self.value_dense = tfkl.Dense(embed_dim, **shared_args)
        self.combine_heads = tfkl.Dense(embed_dim, **shared_args)
        self.attention_dropout = tfkl.Dropout(dropout)

    def attention(self, query, key, value):
        if self.class_attention:
            dim_key = tf.cast(tf.shape(key)[-1], self.compute_dtype)
            query = query / tf.math.sqrt(dim_key)
            scaled_score = tf.matmul(query, key, transpose_b=True)
        else:
            score = tf.matmul(query, key, transpose_b=True)
            dim_key = tf.cast(tf.shape(key)[-1], self.compute_dtype)
            scaled_score = score / tf.math.sqrt(dim_key)

            scaled_score = tf.einsum(self._talking_heads_equation, scaled_score,
                                     self._pre_softmax_weight)

        weights = tf.nn.softmax(scaled_score, axis=-1)

        if not self.class_attention:
            weights = tf.einsum(self._talking_heads_equation, weights,
                                self._post_softmax_weight)

        # This is actually dropping out entire tokens to attend to, which might
        # seem a bit unusual, but is taken from the original Transformer paper.
        # See also the official Google Implementation:
        # https://bit.ly/3wjsxEE

        weights = self.attention_dropout(weights)
        output = tf.matmul(weights, value)

        # For mixed_precision to work with the talking heads weights,
        # we need to manually add the l2 regu ... I think this is a bug in tf
        if not self.class_attention:
            talking_heads_reg_loss_pre = self.l2 * tf.nn.l2_loss(self._pre_softmax_weight)
            talking_heads_reg_loss_post = self.l2 * tf.nn.l2_loss(self._post_softmax_weight)
            self.add_loss(talking_heads_reg_loss_pre + talking_heads_reg_loss_post)

        return output, weights

    def separate_heads(self, x, batch_size):
        x = tf.reshape(
            x, (batch_size, -1, self.num_heads, self.projection_dim)
        )
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, inputs, training=None, **kwargs):
        batch_size = tf.shape(inputs)[0]

        if self.class_attention:
            query = self.query_dense(inputs[:, :2])
        else:
            query = self.query_dense(inputs)

        key = self.key_dense(inputs)
        value = self.value_dense(inputs)

        query = self.separate_heads(query, batch_size)
        key = self.separate_heads(key, batch_size)
        value = self.separate_heads(value, batch_size)

        attention, weights = self.attention(query, key, value)
        attention = tf.transpose(attention, perm=[0, 2, 1, 3])

        concat_attention = tf.reshape(
            attention, (batch_size, -1, self.embed_dim)
        )
        output = self.combine_heads(concat_attention)
        return output


class MultiHeadSelfXCA(tf.keras.layers.Layer):
    def __init__(self, embed_dim=768, num_heads=12, l2=None, dropout=None):
        """
        Cross-Covariance Attention (XCA) operation where the channels are updated using a weighted
        sum. The weights are obtained from the (softmax normalized) Cross-covariance
        matrix (Q^T K \\in d_h \\times d_h)
        """
        super(MultiHeadSelfXCA, self).__init__()
        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(l2)}

        self.embed_dim = embed_dim
        self.num_heads = num_heads

        if embed_dim % num_heads != 0:
            raise ValueError(
                f"embedding dimension = {embed_dim} should be divisible by number of heads = {num_heads}"
            )
        self.projection_dim = embed_dim // num_heads
        self.query_dense = tfkl.Dense(embed_dim, **shared_args)
        self.key_dense = tfkl.Dense(embed_dim, **shared_args)
        self.value_dense = tfkl.Dense(embed_dim, **shared_args)
        self.combine_heads = tfkl.Dense(embed_dim, **shared_args)
        self.attention_dropout = tfkl.Dropout(dropout)

    def attention(self, query, key, value):
        query = tf.math.l2_normalize(tf.transpose(query, perm=[0, 1, 3, 2]), -1)
        key = tf.math.l2_normalize(tf.transpose(key, perm=[0, 1, 3, 2]), -1)
        value = tf.transpose(value, perm=[0, 1, 3, 2])

        score = tf.matmul(query, key, transpose_b=True)
        weights = tf.nn.softmax(score, axis=-1)

        # This is actually dropping out entire tokens to attend to, which might
        # seem a bit unusual, but is taken from the original Transformer paper.
        # See also the official Google Implementation:
        # https://bit.ly/3wjsxEE
        weights = self.attention_dropout(weights)
        output = tf.matmul(weights, value)

        return output, weights

    def separate_heads(self, x, batch_size):
        x = tf.reshape(
            x, (batch_size, -1, self.num_heads, self.projection_dim)
        )
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, inputs, training=None, **kwargs):
        batch_size = tf.shape(inputs)[0]

        query = self.query_dense(inputs)
        key = self.key_dense(inputs)
        value = self.value_dense(inputs)

        query = self.separate_heads(query, batch_size)
        key = self.separate_heads(key, batch_size)
        value = self.separate_heads(value, batch_size)

        attention, weights = self.attention(query, key, value)
        attention = tf.transpose(attention, perm=[0, 2, 1, 3])

        concat_attention = tf.reshape(
            attention, (batch_size, -1, self.embed_dim)
        )
        output = self.combine_heads(concat_attention)
        return output


class TransformerBlock(tf.keras.layers.Layer):
    def __init__(self, embed_dim, num_heads, mlp_size, l2,
                 dropout=0., class_attention=False, epsilon=.1, drop_rate=0.05):
        super(TransformerBlock, self).__init__()
        self.epsilon1 = self.add_weight("epsilon1", shape=[embed_dim],
                                        initializer=tf.keras.initializers.Constant(value=epsilon),
                                        trainable=True,
                                        dtype=self.variable_dtype)
        self.epsilon2 = self.add_weight("epsilon2", shape=[embed_dim],
                                        initializer=tf.keras.initializers.Constant(value=epsilon),
                                        trainable=True,
                                        dtype=self.variable_dtype)
        self.stochastic_depth = nn.StochasticDepth(drop_rate)
        self.layernorm1 = tfkl.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tfkl.LayerNormalization(epsilon=1e-6)
        self.class_attention = class_attention

        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(l2)}

        self.att = MultiHeadSelfAttention(embed_dim=embed_dim, num_heads=num_heads,
                                          l2=l2, dropout=dropout,
                                          class_attention=self.class_attention)

        self.mlp = tf.keras.Sequential(
            [
                tfkl.Dense(mlp_size, activation=tf.nn.gelu, **shared_args),
                tfkl.Dropout(dropout),
                tfkl.Dense(embed_dim, **shared_args),
                tfkl.Dropout(dropout),
            ]
        )

    def call(self, inputs, training=None, **kwargs):
        if self.class_attention:
            x_cls, x = inputs[0], inputs[1]
            u = tf.concat((x_cls, x), 1)
        else:
            u = inputs

        inputs_norm = self.layernorm1(u, training=training)
        attn_output = self.att(inputs_norm, training=training)

        if self.class_attention:
            out1 = self.stochastic_depth([x_cls, self.epsilon1 * attn_output],
                                         training=training)
        else:
            out1 = self.stochastic_depth([inputs, self.epsilon1 * attn_output],
                                         training=training)

        out1_norm = self.layernorm2(out1, training=training)
        mlp_output = self.mlp(out1_norm, training=training)
        return self.stochastic_depth([out1, self.epsilon2 * mlp_output],
                                     training=training)


class XCABlock(tf.keras.layers.Layer):
    def __init__(self, embed_dim, num_heads, mlp_size, l2,
                 dropout=0., epsilon=.1, drop_rate=0.05):
        super(XCABlock, self).__init__()
        self.embed_dim = embed_dim
        self.epsilon1 = self.add_weight("epsilon1", shape=[embed_dim],
                                        initializer=tf.keras.initializers.Constant(value=epsilon),
                                        trainable=True, dtype=self.variable_dtype)
        self.epsilon2 = self.add_weight("epsilon2", shape=[embed_dim],
                                        initializer=tf.keras.initializers.Constant(value=epsilon),
                                        trainable=True, dtype=self.variable_dtype)
        self.epsilon3 = self.add_weight("epsilon3", shape=[embed_dim],
                                        initializer=tf.keras.initializers.Constant(value=epsilon),
                                        trainable=True, dtype=self.variable_dtype)
        self.stochastic_depth = nn.StochasticDepth(drop_rate)
        self.layernorm1 = tfkl.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tfkl.LayerNormalization(epsilon=1e-6)
        self.layernorm3 = tfkl.LayerNormalization(epsilon=1e-6)

        shared_args = {"use_bias": True,
                       "kernel_initializer": tfk.initializers.TruncatedNormal(stddev=0.02),
                       "activity_regularizer": tfk.regularizers.L2(l2)}

        self.att = MultiHeadSelfXCA(embed_dim=embed_dim, num_heads=num_heads,
                                    l2=l2, dropout=dropout)

        self.mlp = tf.keras.Sequential(
            [
                tfkl.Dense(mlp_size, activation=tf.nn.gelu, **shared_args),
                tfkl.Dropout(dropout),
                tfkl.Dense(embed_dim, **shared_args),
                tfkl.Dropout(dropout),
            ]
        )

        # Local Patch Interaction module that allows explicit communication between tokens in 3x3 windows
        # to augment the implicit communcation performed by the block diagonal scatter attention.
        # Implemented using 2 layers of separable 3x3 convolutions with GeLU and BatchNorm2d
        self.LPI = tf.keras.Sequential(
            [
                tfkl.ZeroPadding2D(),
                tfkl.Conv2D(filters=embed_dim, kernel_size=3,
                            activation=tf.nn.gelu, groups=embed_dim,
                            **shared_args),
                tfkl.BatchNormalization(),
                tfkl.ZeroPadding2D(),
                tfkl.Conv2D(filters=embed_dim, kernel_size=3,
                            groups=embed_dim, **shared_args),
            ]
        )

    def call(self, inputs, training=None, **kwargs):
        inputs_norm = self.layernorm1(inputs, training=training)
        out1 = self.stochastic_depth([inputs, self.epsilon1 * self.att(inputs_norm, training=training)],
                                     training=training)

        out1_norm = self.layernorm2(out1, training=training)
        out1_shape = tf.shape(out1_norm)
        conv_height = tf.cast(tf.math.sqrt(tf.cast(out1_shape[1], tf.float32)), tf.int32)
        out1_norm = tf.reshape(out1_norm, [-1, conv_height, conv_height, self.embed_dim])
        lpi_out = self.LPI(out1_norm, training=training)
        lpi_out = tf.reshape(lpi_out, out1_shape)
        out2 = self.stochastic_depth([out1, self.epsilon2 * lpi_out],
                                     training=training)

        out2_norm = self.layernorm2(out1, training=training)
        out3 = self.stochastic_depth([out2, self.epsilon3 * self.mlp(out2_norm, training=training)],
                                     training=training)

        return out3


def xcit_tst(input_shape, num_classes, layers=4, num_cait_layers=2, hidden_size=192, mlp_size=768,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-6, xcit=False,
             drop_rate=0.05, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, xcit=xcit, name="xcit_tst", drop_rate=drop_rate, **kwargs)


def cait_xxs24(input_shape, num_classes, layers=24, num_cait_layers=2, hidden_size=192, mlp_size=768,
               heads=4, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-5, drop_rate=0.05, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_xxs24", **kwargs)


def cait_xs36(input_shape, num_classes, layers=36, num_cait_layers=2, hidden_size=288, mlp_size=1152,
              heads=6, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-6, drop_rate=0.1, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_xs36", **kwargs)


def xcit_s12(input_shape, num_classes, layers=12, num_cait_layers=2, hidden_size=384, mlp_size=1536,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=0.1, xcit=True, drop_rate=0.05,
             **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, xcit=xcit, name="xcit_s12", drop_rate=drop_rate, **kwargs)


def cait_s24(input_shape, num_classes, layers=24, num_cait_layers=2, hidden_size=384, mlp_size=1536,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-5, drop_rate=0.1, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_s24", **kwargs)


def xcit_s24(input_shape, num_classes, layers=24, num_cait_layers=2, hidden_size=384, mlp_size=1536,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-5, xcit=True, drop_rate=0.05,
             **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, xcit=xcit, name="xcit_s24", drop_rate=drop_rate, **kwargs)


def cait_s36(input_shape, num_classes, layers=36, num_cait_layers=2, hidden_size=384, mlp_size=1536,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-6, drop_rate=0.2, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_s36", **kwargs)


def cait_m24(input_shape, num_classes, layers=24, num_cait_layers=2, hidden_size=768, mlp_size=3072,
             heads=16, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-5, drop_rate=0.2, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_m24", **kwargs)


def xcit_m24(input_shape, num_classes, layers=24, num_cait_layers=2, hidden_size=512, mlp_size=2048,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-5, xcit=True, drop_rate=0.05,
             **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, xcit=xcit, name="xcit_m24", drop_rate=drop_rate, **kwargs)


def cait_s48(input_shape, num_classes, layers=48, num_cait_layers=2, hidden_size=384, mlp_size=1536,
             heads=8, pretrain=False, l2=1e-4, dropout=0., patch_size=16, epsilon=1e-6, drop_rate=0.3, **kwargs):
    return ViT(input_shape=input_shape, num_classes=num_classes, pretrain=pretrain,
               layers=layers, num_cait_layers=num_cait_layers, hidden_size=hidden_size,
               mlp_size=mlp_size, heads=heads, l2=l2, dropout=dropout, epsilon=epsilon,
               patch_size=patch_size, drop_rate=drop_rate, name="cait_s48", **kwargs)
