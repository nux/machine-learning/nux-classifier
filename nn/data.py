import resource

low, high = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (high, high))
import nn
import tensorflow as tf
import squirrel as sq
from nn import check_split_string

tfk = tf.keras
tfkl = tfk.layers
tfklpre = tfkl.experimental.preprocessing


# noinspection PyProtectedMember
def prep_ds(ds_single, args, ty="pretrain_train", info=None):
    """
    Prepare the dataset
    :param ds_single: Tensorflow dataset
    :param args: The args dataframe
    :param ty: Either 'pretrain_eval', 'pretrain_train', 'eval', or 'train'
    :param info: Should we return the info dataframe. 'True' or 'False'
    :return: a Tensorflow dataset
    """
    # we want the info frame
    assert info is not None

    # if we deal with a helium dataset, we need to do things differently
    if "train" in ty.lower():
        if "helium" in info.splits["train"]._dataset_name:
            shuffle_size = check_split_string(args.split_list[1], info)
        else:
            shuffle_size = info.splits["train"].num_examples
    elif "eval" in ty.lower():
        if "helium" in info.splits["train"]._dataset_name:
            shuffle_size = check_split_string(args.split_list[0], info)
        else:
            shuffle_size = info.splits["validation" if args.dataset == "imagenet2012" else "test"].num_examples
    else:
        raise LookupError("ty string doesn't contain 'train' or 'eval': {}".format(ty))

    # Cache, shuffle, and batch everything
    img_shape = (args.img_size, args.img_size, args.img_channel)
    if args.dataset == "imagenet2012":
        ds_single = ds_single.map(lambda x, y: (nn.resize_img(x, img_shape, False), y),
                                  num_parallel_calls=tf.data.AUTOTUNE)

    shuffle_size = min(shuffle_size, 2e4)
    ds = nn.cache_shuffle_batch(ds_single, shuffle_size, args)

    # Pre Normalize 0 and 1
    ds = ds.map(lambda img, lbl: nn.normalize_img(img, lbl, use_image_normalization=False),
                num_parallel_calls=tf.data.AUTOTUNE)

    if args.use_mixup and ty == "train":
        # Because we will be mixing up the images and their corresponding labels, we will be
        # combining two shuffled datasets from the same training data.
        ds_two_ = nn.cache_shuffle_batch(ds_single, shuffle_size, args)
        ds_two_ = ds_two_.map(lambda img, lbl: nn.normalize_img(img, lbl, use_image_normalization=False),
                              num_parallel_calls=tf.data.AUTOTUNE)

        train_ds = tf.data.Dataset.zip((ds, ds_two_))

        ds = train_ds.map(
            lambda ds_one, ds_two: nn.mix_up(ds_one, ds_two, alpha=args.mixup_alpha),
            num_parallel_calls=tf.data.AUTOTUNE
        )

    if "helium" in info.splits["train"]._dataset_name:
        # Expand Channels
        ds = ds.map(nn.expand_channels, num_parallel_calls=tf.data.AUTOTUNE)
        # Cut out the center
        ds = ds.map(nn.center_cutout, num_parallel_calls=tf.data.AUTOTUNE)

    if "train" in ty:
        kwargs = dict(img_dims=img_shape, pretrain=args.pretrain,
                      resize=False if args.dataset == "imagenet2012" else True)
    elif "eval" in ty:
        kwargs = dict(img_dims=img_shape, randaug=False, pretrain=args.pretrain,
                      crop=False, color_distort=False, flip=False,
                      resize=False if args.dataset == "imagenet2012" else True)
    else:
        raise LookupError("Training mode not specified; Should be either of "
                          "'pretrain_eval', 'pretrain_train', 'eval', or 'train', "
                          "but is: {}".format(ty))

    # In case of pretraining we concat the image batch along the channel
    # dimension, so that we feed two images for every sample in the batch
    ds = ds.map(lambda x, y: nn.base_augment(x, y, **kwargs),
                num_parallel_calls=tf.data.AUTOTUNE)

    if args.pretrain:
        ds = ds.map(lambda x, y: (tf.stop_gradient(
            tf.py_function(nn.distort_images, [x], tf.float32)
        ), y), num_parallel_calls=tf.data.AUTOTUNE)

    # Normalize to have zero mean and unity variance
    ds = ds.map(nn.normalize_img, num_parallel_calls=tf.data.AUTOTUNE)
    ds = ds.prefetch(tf.data.AUTOTUNE)
    return ds, shuffle_size


def get_data(name, args):
    """
    Unified data pipeline
    :param name: Name of the dataset, see sq.list_builders()
    :param args: The args dataframe
    :return:
    """
    assert name in sq.list_builders()

    (test, train), info = sq.load(name=name,
                                  split=args.split_list,
                                  with_info=True,
                                  as_supervised=True)

    if args.pretrain:
        eval_ty = "pretrain_eval"
        train_ty = "pretrain_train"
    else:
        eval_ty = "eval"
        train_ty = "train"

    test, num_examples_test = prep_ds(ds_single=test, args=args,
                                      ty=eval_ty, info=info)
    train, num_examples_train = prep_ds(ds_single=train, args=args,
                                        ty=train_ty, info=info)

    return (test, train), info, num_examples_train, num_examples_test
