# coding=utf-8
# Copyright 2020 The SimCLR Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific simclr governing permissions and
# limitations under the License.
# ==============================================================================
"""Data preprocessing and augmentation."""

import io
import math
import functools
import tensorflow as tf
import matplotlib.pyplot as plt
from imgaug.augmenters import RandAugment
import tensorflow_probability as tfp

tfd = tfp.distributions


def check_split_string(splstr, info):
    """
    This extracts a multiplier factor from the split list string
    :param splstr: The split string passed to the training routine
    :param info: The info frame provided by Squirrel
    :return:
    """
    assert isinstance(splstr, str)

    # Get the index
    if "[" in splstr and "]" in splstr:
        ty = splstr[:splstr.find("[")]
        idx_str = splstr[splstr.find("[") + 1:splstr.find("]")]
        if ":" == idx_str[0]:
            first = True
            val_str = idx_str[1:]
        else:
            first = False
            val_str = idx_str[:-1]

        if "%" in splstr:
            factor = int(val_str.replace("%", "")) / 100
            if not first:
                factor = 1 - factor
            num_examples = round(info.splits[ty].num_examples * factor)
        else:
            num_examples = int(val_str)
            if not first:
                num_examples = info.splits[ty].num_examples - num_examples
            # factor = num_examples / info.splits[ty].num_examples

        return num_examples
    else:
        raise LookupError("'[' and ']' not in splstr")


def random_apply(func, p, x):
    """Randomly apply function func to x with probability p."""
    return tf.cond(
        tf.less(
            tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32),
            tf.cast(p, tf.float32)), lambda: func(x), lambda: x)


def random_brightness(image, max_delta, impl='simclrv2'):
    """A multiplicative vs additive change of brightness."""
    if impl == 'simclrv2':
        factor = tf.random.uniform([image.shape[0]], tf.maximum(1.0 - max_delta, 0),
                                   1.0 + max_delta)
        image = tf.transpose(tf.transpose(image) * factor)
    elif impl == 'simclrv1':
        image = tf.image.random_brightness(image, max_delta=max_delta)
    else:
        raise ValueError('Unknown impl {} for random brightness.'.format(impl))
    return image


def to_grayscale(image, keep_channels=True):
    image = tf.image.rgb_to_grayscale(image)
    if keep_channels:
        image = tf.tile(image, [1, 1, 1, 3])
    return image


def color_jitter(image, strength, random_order=True, impl='simclrv2',
                 brightness_flag=True, contrast_flag=True,
                 saturation_flag=True, hue_flag=True):
    """Distorts the color of the image.

    Args:
      image: The input image tensor.
      strength: the floating number for the strength of the color augmentation.
      random_order: A bool, specifying whether to randomize the jittering order.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.
      brightness_flag: Boolean if brightness should be adjusted
      contrast_flag: Boolean if contrast should be adjusted
      saturation_flag: Boolean if saturation should be adjusted
      hue_flag: Boolean if hue should be adjusted

    Returns:
      The distorted image tensor.
    """
    if brightness_flag:
        brightness = strength
    else:
        brightness = 0
    if contrast_flag:
        contrast = strength
    else:
        contrast = 0
    if saturation_flag:
        saturation = strength
    else:
        saturation = 0
    if hue_flag:
        hue = strength
    else:
        hue = 0

    if random_order:
        return color_jitter_rand(
            image, brightness, contrast, saturation, hue, impl=impl)
    else:
        return color_jitter_nonrand(
            image, brightness, contrast, saturation, hue, impl=impl)


def color_jitter_nonrand(image,
                         brightness=0,
                         contrast=0,
                         saturation=0,
                         hue=0,
                         impl='simclrv2'):
    """Distorts the color of the image (jittering order is fixed).

    Args:
      image: The input image tensor.
      brightness: A float, specifying the brightness for color jitter.
      contrast: A float, specifying the contrast for color jitter.
      saturation: A float, specifying the saturation for color jitter.
      hue: A float, specifying the hue for color jitter.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.

    Returns:
      The distorted image tensor.
    """
    with tf.name_scope('distort_color'):
        def apply_transform(i, x, brightness, contrast, saturation, hue):
            """Apply the i-th transformation."""
            if brightness != 0 and i == 0:
                x = random_brightness(x, max_delta=brightness, impl=impl)
            elif contrast != 0 and i == 1:
                x = tf.image.random_contrast(
                    x, lower=1 - contrast, upper=1 + contrast)
            elif saturation != 0 and i == 2:
                x = tf.image.random_saturation(
                    x, lower=1 - saturation, upper=1 + saturation)
            elif hue != 0:
                x = tf.image.random_hue(x, max_delta=hue)
            return x

        for i in range(4):
            image = apply_transform(i, image, brightness, contrast, saturation, hue)
            image = tf.clip_by_value(image, 0., 1.)
        return image


def color_jitter_rand(image,
                      brightness=0,
                      contrast=0,
                      saturation=0,
                      hue=0,
                      impl='simclrv2'):
    """Distorts the color of the image (jittering order is random).

    Args:
      image: The input image tensor.
      brightness: A float, specifying the brightness for color jitter.
      contrast: A float, specifying the contrast for color jitter.
      saturation: A float, specifying the saturation for color jitter.
      hue: A float, specifying the hue for color jitter.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.

    Returns:
      The distorted image tensor.
    """
    with tf.name_scope('distort_color'):
        def apply_transform(i, x):
            """Apply the i-th transformation."""

            def brightness_foo():
                if brightness == 0:
                    return x
                else:
                    return random_brightness(x, max_delta=brightness, impl=impl)

            def contrast_foo():
                if contrast == 0:
                    return x
                else:
                    return tf.image.random_contrast(x, lower=1 - contrast, upper=1 + contrast)

            def saturation_foo():
                if saturation == 0:
                    return x
                else:
                    return tf.image.random_saturation(
                        x, lower=1 - saturation, upper=1 + saturation)

            def hue_foo():
                if hue == 0:
                    return x
                else:
                    return tf.image.random_hue(x, max_delta=hue)

            x = tf.cond(tf.less(i, 2),
                        lambda: tf.cond(tf.less(i, 1), brightness_foo, contrast_foo),
                        lambda: tf.cond(tf.less(i, 3), saturation_foo, hue_foo))
            return x

        perm = tf.random.shuffle(tf.range(4))
        for i in range(4):
            image = apply_transform(perm[i], image)
            image = tf.clip_by_value(image, 0., 1.)
        return image


def random_color_jitter(image, p=1.0, strength=1.0, impl='simclrv2'):
    def _transform(image):
        color_jitter_t = functools.partial(
            color_jitter, strength=strength, impl=impl, hue_flag=False)
        # return random_apply(to_grayscale, p=0.2, x=image)
        return color_jitter_t(image)

    return random_apply(_transform, p=p, x=image)


def expand_channels(img, label):
    image = tf.image.grayscale_to_rgb(img)
    return image, label


def expand_channels_sobel(img, lbl):
    img_conc = tf.expand_dims(img, -1)
    img = tf.expand_dims(tf.expand_dims(img, 0), -1)

    sobel_images = tf.squeeze(tf.image.sobel_edges(img))
    img = tf.concat([img_conc, sobel_images], axis=-1)
    return img, lbl


def center_cutout(img, label):
    # This cuts out the central part of the image so that
    # the height and width of the cropped area are fully
    # within the diffraction pattern.
    image = tf.image.central_crop(img, math.cos(math.pi / 4))
    return image, label


def normalize_img(x, lbl=None, new_max=1., new_min=0., use_image_normalization=True, to_uint8=False):
    """
    Linearly interpolate a batch of images between "new_max" and "new_min"
    :param x: Input -> Will be casted to float32
    :param lbl: Label input in order to be used with tfds
    :param new_max: New maximum of x
    :param new_min: New minimum of x
    :param use_image_normalization: centering around zero, adjusting std to be one
    :param to_uint8: convert to uint8, scale between [0, 255]
    :return: Scaled and casted version of x
    """

    x_shape = tf.shape(x)
    bs = x_shape[0]
    x_cast = tf.cast(x, tf.float32)
    outer_dim = tf.math.reduce_prod(x_shape[1:])
    x_cast_reshaped = tf.reshape(x_cast, [bs, outer_dim])
    current_min = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_min(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)
    current_max = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_max(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)
    if to_uint8:
        max_8 = tf.broadcast_to(tf.cast(255, tf.float32), [bs, outer_dim])
        scaled_x = max_8 * (x_cast_reshaped - current_min) / (current_max - current_min)
        scaled_x = tf.reshape(scaled_x, x_shape)
        return tf.image.convert_image_dtype(scaled_x, dtype=tf.uint8), lbl

    if use_image_normalization:
        current_mean = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_mean(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        current_std = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_std(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        updates = 1.0 / tf.math.sqrt(tf.cast(outer_dim, tf.float32))
        cond = tf.math.greater(current_std, updates)
        adjusted_stddev = tf.where(cond, current_std, updates)
        scaled_x = (x_cast_reshaped - current_mean) / adjusted_stddev

    else:
        new_max_cast = tf.broadcast_to(tf.cast(new_max, tf.float32), [bs, outer_dim])
        new_min_cast = tf.broadcast_to(tf.cast(new_min, tf.float32), [bs, outer_dim])
        scaled_x = new_min_cast + (new_max_cast - new_min_cast) * \
                   (x_cast_reshaped - current_min) / (current_max - current_min)

    scaled_x = tf.reshape(scaled_x, x_shape)
    return scaled_x, lbl


def resize_img(x_, img_dims_out, batched=True):
    _in_shape = tf.shape(x_)
    out_tuple = (img_dims_out[0], img_dims_out[1])
    if batched:
        _in_size = [_in_shape[1], _in_shape[2]]
        _in_bs = _in_shape[0]
    else:
        _in_size = [_in_shape[0], _in_shape[1]]
        _in_bs = None

    # if the min of in_size is still larger than the max of the desired output size
    # then do zoom in the image for resizing, but downsize it so that the subsequent crop
    # gets as much image information as possible
    # dim_in = tf.reduce_max(_in_size)
    dim_out = tf.reduce_max(out_tuple)

    scale_in = tf.reduce_max(_in_size) / tf.reduce_min(_in_size)
    rescale = tf.math.ceil(
        tf.math.multiply(tf.cast(dim_out, tf.float32),
                         tf.cast(scale_in, tf.float32)))
    x_ = tf.image.resize(x_, (rescale, rescale), preserve_aspect_ratio=True,
                         method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    if batched:
        crop_tuple = (_in_bs, img_dims_out[0], img_dims_out[1], img_dims_out[2])
    else:
        crop_tuple = img_dims_out
    x_ = tf.image.random_crop(x_, crop_tuple)
    return x_


def base_augment(images, labels,
                 img_dims,
                 crop=True,
                 color_distort=True,
                 flip=True,
                 pretrain=False,
                 randaug=True,
                 resize=True):
    """
    From:
    Atito, S., Awais, M. & Kittler, J.
    SiT: Self-supervised vIsion Transformer. (2021)

    Simple data augmentation techniques are applied during the self-supervised training.
    We found that to learn low- level features as well as high-level semantic information,
    aggressive data augmentation like MixUp [44] and Auto- Augment [45] hurts the training,
    specially with the objective functions in hand. Therefore, we used only cropping,
    colour jittering and horizontal flipping by selecting a random patch from the image
    and resizing it to 224 × 224 with a random horizontal flip. After data augmentation,
    image distortion techniques described in Section 3 are applied to the perturbed
    image and the network is optimised together with the rotation prediction and
    contrastive learning to reconstruct the image after distortion.

    Args:
      images: `Tensor` representing an image of arbitrary size.
      labels: `Tensor` representing the labels of arbitrary size. (Not needed during pretrain)
      img_dims: Dimensions of the output image.
      crop: Whether to crop the image.
      color_distort: Whether to random color jitter
      flip: Whether or not to flip left and right of an image.
      pretrain: Additional pretrain augmentation
      randaug: use RandAugment
      resize: should we resize ... this is either done on the batch (non-imagenet)
                of individually (imagenet) outside of this routine

    Returns:
      A preprocessed image `Tensor`.
    """

    def manual_augment(x_):
        if crop:
            # resizing by 12 %
            new_size = tf.cast(tf.cast(in_shape[1], tf.float32) * 1.12, tf.int32)
            x_ = tf.image.resize(x_, [new_size, new_size],
                                 method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

            # randomly cropping
            x_ = tf.image.random_crop(x_, in_shape)
        if color_distort:
            x_ = random_color_jitter(x_, strength=.4, impl="simclrv2")
        if flip:
            x_ = tf.image.random_flip_left_right(x_)

        return x_

    def rand_augment(img):
        rand_aug = RandAugment(n=2, m=(7, 11))

        def augment(x):
            # Input to `augment()` is a TensorFlow tensor which
            # is not supported by `imgaug`. This is why we first
            # convert it to its `numpy` variant.
            x = tf.image.convert_image_dtype(x, dtype=tf.uint8)
            x = rand_aug(images=x.numpy())
            return x

        img = tf.py_function(augment, [img], [tf.uint8])[0]
        # img = tf.image.convert_image_dtype(img, dtype=tf.float32)
        return img

    if randaug and not pretrain:  # RandAug only during finetune or scratch train
        fun = rand_augment
    else:
        fun = manual_augment

    if resize:
        images = resize_img(images, img_dims)

    in_shape = tf.shape(images)
    if pretrain:
        def rotate_image(arg):
            x = tf.image.rot90(arg[0], k=arg[1])
            return x

        # Apply the set of base augmentations
        images1 = fun(images)
        images2 = fun(images)
        # Rotate all images randomly
        labels = tf.random.uniform([int(2 * in_shape[0])], maxval=4, dtype=tf.int32, seed=1)
        images = tf.vectorized_map(rotate_image, [tf.concat([images1, images2], 0), labels])

        [images1, images2] = tf.split(images, num_or_size_splits=2, axis=0)
        images = tf.concat([images1, images2], -1)
        labels = tf.one_hot(labels, 4)

    else:
        images = fun(images)

    return images, labels


def build_lbl_pred_png(lbls, preds, probs):
    # Build a nice matplotlib figure
    f, ax = plt.subplots(3, 1, figsize=(10, 5))
    ax[0].imshow(tf.transpose(lbls), cmap="gray")
    ax[0].text(0, 1.1, "Original labels", transform=ax[0].transAxes)
    ax[1].imshow(tf.transpose(preds), cmap="gray")
    ax[1].text(0, 1.1, "Predictions", transform=ax[1].transAxes)
    ax[2].imshow(tf.transpose(probs), cmap="gray")
    ax[2].text(0, 1.1, "Probabilities", transform=ax[2].transAxes)
    for a in ax:
        a.axis(False)
    f.tight_layout()

    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close(f)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


def without_keys(d, keys):
    return {x: d[x] for x in d if x not in keys}


def cache_shuffle_batch(dataset, sh_size, args):
    options = tf.data.Options()
    options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.AUTO
    dataset = dataset.with_options(options)

    dataset = dataset.cache()
    # For true randomness, we set the shuffle buffer to the full dataset size.
    dataset = dataset.shuffle(tf.cast(sh_size, tf.int64))
    # Make batches
    dataset = dataset.batch(args.batch_size, drop_remainder=True)

    return dataset


def patches_to_images(arg, arg_shape, box_size):
    patches_ori = tf.reshape(arg, arg_shape)
    patches_rec = tf.nn.depth_to_space(patches_ori, box_size)
    return patches_rec


def images_to_patches(arg, bs, size, channels, filter_size):
    patches = tf.nn.space_to_depth(arg, int(filter_size))
    patch_shape = tf.shape(patches)
    num_patches = size / filter_size
    new_shape = [int(float(bs) * float(num_patches) * float(num_patches)),
                 int(filter_size), int(filter_size), int(channels)]
    patches = tf.reshape(patches, new_shape)

    return patches, patch_shape


def get_random_filter_sizes(size, odd=False):
    start_size = size // 10
    if start_size == 0:
        start_size = 1
    possible_filter_sizes = tf.range(start_size, size // 2, 1, dtype=tf.int32)
    good_sizes = tf.where(tf.math.floormod(size, possible_filter_sizes) == 0)
    valid_kernel_sizes = tf.gather(possible_filter_sizes,
                                   good_sizes)

    rnd_kernel_size_idx = tf.random.uniform([1],
                                            minval=0,
                                            maxval=tf.shape(valid_kernel_sizes)[0],
                                            dtype=tf.int32, seed=1)

    filter_size = tf.gather(valid_kernel_sizes, rnd_kernel_size_idx)[0]
    if odd:
        filter_size = 2 * (filter_size // 2) + 1
    return filter_size[0].numpy()


def get_random_idx(num_patches, batch_size, shape=False, even=False):
    if shape:
        idx_shape = shape
    else:
        idx_shape = tf.random.uniform(shape=[1], minval=batch_size,
                                      maxval=num_patches // 5, dtype=tf.int32)
        if even:
            idx_shape = 2 * (idx_shape // 2)
    idxs = tf.random.uniform(shape=idx_shape, minval=0,
                             maxval=num_patches, dtype=tf.int32)
    return idxs


def random_grey(patches, num_patches, batch_size, channel):
    idxs = get_random_idx(num_patches, batch_size)
    patch_gathered = tf.gather(patches, idxs)
    patch_bw_single_dim = tf.expand_dims(tf.reduce_mean(patch_gathered, axis=-1), -1)
    patch_bw = tf.repeat(patch_bw_single_dim,
                         channel, 3)
    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch_bw)
    return patches


def random_drop_replace(patches, num_patches, batch_size):
    idxs = get_random_idx(num_patches, batch_size, even=True)
    idxs_replace, idxs_noise = tf.split(idxs, 2)

    patch_gathered_noise = tf.gather(patches, idxs_noise)
    idxs_replace_new = get_random_idx(num_patches, batch_size, tf.shape(idxs_replace))
    patch_replace = tf.gather(patches, idxs_replace_new)

    patch_noise = tfd.Uniform().sample(tf.shape(patch_gathered_noise))
    patch = tf.concat([patch_replace, patch_noise], 0)
    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch)
    return patches


def gaussian_kernel(size: int,
                    std: float,
                    ):
    """
    Makes 2D gaussian Kernel for convolution.
    """
    d = tfd.Normal(loc=0, scale=std)

    prob_range = tf.range(start=0, limit=size, dtype=tf.float32) - float(size) // 2
    vals = tf.expand_dims(d.prob(prob_range), -1)
    gauss_kernel = tf.matmul(vals, vals,
                             transpose_b=True)

    return gauss_kernel / tf.reduce_sum(gauss_kernel)


def random_blur(patches, num_patches, batch_size, channels, patch_size, max_sigma=5):
    idxs = get_random_idx(num_patches, batch_size)
    patch_gathered = tf.gather(patches, idxs)
    patch_gathered_shape = tf.shape(patch_gathered)
    size = get_random_filter_sizes(patch_size, odd=True)
    rnd_sigma_size = tf.random.uniform([1],
                                       minval=.1,
                                       maxval=max_sigma + .1,
                                       dtype=tf.float32, seed=1)

    gauss_kernel = gaussian_kernel(size=size,
                                   std=rnd_sigma_size ** 2)

    # Expand dimensions of `gauss_kernel` for `tf.nn.conv2d` signature
    # print(patch_size, size, gauss_kernel.shape)
    gauss_kernel = tf.expand_dims(tf.expand_dims(gauss_kernel[:, :], -1), -1)
    gauss_kernel = tf.broadcast_to(gauss_kernel,
                                   (size, size, int(channels), 1))

    # Convolve
    patch_blurred = tf.reshape(patch_gathered, [patch_gathered_shape[0], patch_size, patch_size, int(channels)])
    pad_size = size // 2
    patch_blurred = tf.pad(patch_blurred,
                           [[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]],
                           "REFLECT")

    patch_blurred = tf.nn.depthwise_conv2d(patch_blurred,
                                           gauss_kernel,
                                           strides=(1, 1, 1, 1),
                                           padding="VALID")

    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch_blurred)
    return patches


def distort_images(images):
    # Special pretrain augmentation as in section 3 of:
    # Atito, S., Awais, M. & Kittler, J.
    # SiT: Self-supervised vIsion Transformer. (2021)

    # random drop and random replace, colour distortions,
    # recolouring, blurring, grey-scale

    # As for the colour transformations, it involves basic adjustments
    # of colour levels in an image including colour distortions,
    # blurring, and converting to grey-scale. Colour distortions are
    # applied to the whole image to enable the network to recognise similar
    # images invariant to their colours. Unlike colour distortions,
    # blurring and conversion to grey-scale transformations are applied
    # to the local neigh- bourhood of arbitrary patches of the image rather
    # than the full image to enable the network to learn the texture and
    # colour transformations from the surrounding pixels. Blurring is
    # performed by applying a Gaussian filter to the selected patches and
    # converting to grey-scale is performed by a linear transformation of
    # the coloured (RGB) patches.
    undistorted_images_list = tf.split(
        images, num_or_size_splits=2, axis=-1)
    sh = tf.shape(undistorted_images_list[0])
    undistorted_images = tf.concat(undistorted_images_list, 0)
    batch_size, height, width, channels = 2 * sh[0], sh[1], sh[2], sh[3]
    filter_size = get_random_filter_sizes(height)

    distorted_images, distorted_images_shape = images_to_patches(undistorted_images, batch_size,
                                                                 height, channels, filter_size)

    num_patches = tf.shape(distorted_images)[0]
    distorted_images = random_grey(distorted_images, num_patches,
                                   batch_size, channels)

    distorted_images = random_blur(distorted_images, num_patches,
                                   batch_size, channels, filter_size)

    distorted_images = random_drop_replace(distorted_images, num_patches,
                                           batch_size)

    distorted_images = patches_to_images(distorted_images, distorted_images_shape, filter_size)

    # Back together
    distorted_images_list = tf.split(
        distorted_images, num_or_size_splits=2, axis=0)
    distorted_images = tf.concat(distorted_images_list, -1)
    return tf.concat([images, distorted_images], -1)


def sample_beta_distribution(size, concentration_0, concentration_1):
    gamma_1_sample = tf.random.gamma(shape=[size], alpha=concentration_1)
    gamma_2_sample = tf.random.gamma(shape=[size], alpha=concentration_0)
    return gamma_1_sample / (gamma_1_sample + gamma_2_sample)


def mix_up(ds_one, ds_two, alpha=0.2):
    # Zhang, H., Cisse, M., Dauphin, Y. N. & Lopez-Paz, D.
    # mixup: Beyond Empirical Risk Minimization.
    # in 6th International Conference on Learning Representations, ICLR 2018
    # (International Conference on Learning Representations, ICLR, 2017).

    # Unpack two datasets
    images_one, labels_one = ds_one
    images_two, labels_two = ds_two

    batch_size = tf.shape(images_one)[0]
    labels_one = tf.cast(tf.one_hot(labels_one, 10), tf.float32)
    labels_two = tf.cast(tf.one_hot(labels_two, 10), tf.float32)

    # Sample lambda and reshape it to do the mixup
    l = sample_beta_distribution(batch_size, alpha, alpha)
    x_l = tf.reshape(l, (batch_size, 1, 1, 1))
    y_l = tf.reshape(l, (batch_size, 1))

    # Perform mixup on both images and labels by combining a pair of images/labels
    # (one from each dataset) into one image/label
    images = images_one * x_l + images_two * (1 - x_l)
    labels = labels_one * y_l + labels_two * (1 - y_l)

    return images, labels
