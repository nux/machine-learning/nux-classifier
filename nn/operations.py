import os
import nn
import re
import math
import datetime
import tensorflow as tf

from abc import ABCMeta
from collections import namedtuple

tfkm = tf.keras.metrics


class StochasticDepth(tf.keras.layers.Layer):
    """Stochastic Depth layer.

    Implements Stochastic Depth as described in
    [Deep Networks with Stochastic Depth](https://arxiv.org/abs/1603.09382), to randomly drop residual branches
    in residual architectures.

    Args:
        drop_rate: float, the drop_rate (1 - survival probability) of the residual branch being kept.

    Call Args:
        inputs:  List of `[shortcut, residual]` where `shortcut`, and `residual` are tensors of equal shape.

    Output shape:
        Equal to the shape of inputs `shortcut`, and `residual`
    """

    def __init__(self, drop_rate: float = 0.5, **kwargs):
        super().__init__(**kwargs)

        self.survival_probability = 1. - drop_rate

    def call(self, x, training=None):
        if not isinstance(x, list) or len(x) != 2:
            raise ValueError("input must be a list of length 2.")

        shortcut, residual = x

        # Random bernoulli variable indicating whether the branch should be kept or not or not
        b_l = tf.keras.backend.random_bernoulli([], p=self.survival_probability)

        def _call_train():
            return shortcut + tf.cast(b_l, self.compute_dtype) * residual

        def _call_test():
            return shortcut + self.survival_probability * residual

        return tf.keras.backend.in_train_phase(
            _call_train, _call_test, training=training
        )

    @staticmethod
    def compute_output_shape(input_shape):
        return input_shape[0]

    def get_config(self):
        base_config = super().get_config()

        config = {"survival_probability": self.survival_probability}

        return {**base_config, **config}


class LinearCosineSchedule(tf.keras.optimizers.schedules.LearningRateSchedule, metaclass=ABCMeta):
    def __init__(self, initial_learning_rate, warmup_steps, total_steps):
        """
        The learning scheduler increases linearly from 0 to "initial_learning_rate" for
        about "warmup_steps" steps. After the warmup, the learning rate decreases by a
        cosine schedule towards zero until the training ends.

        See:
        1. He, T. et al. Bag of Tricks for Image Classification with Convolutional Neural Networks.
        Proc. IEEE Comput. Soc. Conf. Comput. Vis. Pattern Recognit. 2019-June, 558–567 (2018).

        :param initial_learning_rate: The learning rate after the warmup phase
        :param warmup_steps: The number of warmup steps
        :param total_steps: The total number of steps for training
        """
        super(LinearCosineSchedule, self).__init__()

        self.initial_learning_rate = tf.cast(initial_learning_rate, tf.float32)
        self.total_steps = tf.cast(total_steps, tf.float32)
        self.warmup_steps = tf.cast(tf.math.maximum(1, warmup_steps), tf.float32)

    def __call__(self, step):
        arg1 = self.cosine(tf.cast(step, tf.float32) - self.warmup_steps)
        m = tf.math.divide(self.initial_learning_rate, self.warmup_steps)
        arg2 = tf.math.multiply(m, tf.cast(step, tf.float32))

        return tf.math.minimum(arg1, arg2)

    def cosine(self, step):
        cos_arg = tf.math.divide(step, self.total_steps)
        cos_ = tf.math.cos(tf.math.multiply(math.pi, cos_arg))
        mul_lr = tf.math.multiply(.5, 1 + cos_)
        return tf.math.multiply(mul_lr, self.initial_learning_rate)


def str2bool(x):
    return x.lower() in "true"


def check_folder(directory, return_path=False):
    if not os.path.exists(directory):
        os.makedirs(directory)
    if return_path:
        return directory


def set_path_env(args, create_folders=True):
    if args.pretrain:
        train_mode_str = "pretrain"
    else:
        train_mode_str = "finetune"
    # else:
    #     raise LookupError("finetune or pretrain should be defined in the args dictionary")

    suffix = "{}_{}_{}_{}".format(args.dataset,
                                  args.batch_size,
                                  args.model_name,
                                  train_mode_str)

    # Setting up the paths
    path_tuple = namedtuple("path_tuple", "work_dir weight_dir log_dir base_dir")
    if create_folders:
        a = check_folder(args.work_dir, True)
        if hasattr(args, 'paths'):
            base_dir = args.paths.base_dir
        else:
            current_time = datetime.datetime.now().strftime("%d_%m_%Y-%H_%M_%S")
            base_dir = os.path.join(a, current_time + "_" + suffix)
        b = check_folder(os.path.join(base_dir, "weights"), True)
        c = check_folder(os.path.join(base_dir, "logs"), True)
    else:
        a = args.work_dir
        current_time = datetime.datetime.now().strftime("%d_%m_%Y-%H_%M_%S")
        base_dir = os.path.join(a, current_time + "_" + suffix)
        b = os.path.join(base_dir, "weights")
        c = os.path.join(base_dir, "logs")

    paths = path_tuple(work_dir=a, weight_dir=b, log_dir=c, base_dir=base_dir)
    return paths


# noinspection PyProtectedMember
def get_model(args, info):
    # If we use a helium dataset, then we don"t have a dedicated test and train set.
    # We, therefore, have to manually set how many examples are in the test and train dataset
    if "helium" in info.splits["train"]._dataset_name:
        # train_examples = tf.math.round(int(info.splits["train"].num_examples * 0.9))
        # test_examples = tf.math.round(int(info.splits["train"].num_examples * 0.1))
        _, train_examples = nn.check_split_string(args.split_list[1], info)
        _, test_examples = nn.check_split_string(args.split_list[0], info)
        num_classes = int(info.features["label"].shape[0])
    else:
        train_examples = info.splits[args.split_list[1]].num_examples
        test_examples = info.splits[args.split_list[0]].num_examples
        num_classes = info.features["label"].num_classes
    steps_per_epoch = math.floor(train_examples / args.batch_size)
    eval_steps_per_epoch = math.floor(test_examples / args.batch_size)
    total_train_steps = math.floor(steps_per_epoch * args.epochs)

    # Image shape
    img_shape = [args.img_size, args.img_size, args.img_channel]

    if len(img_shape) == 2:
        batch_shape = [None] + img_shape + [1]
    elif len(img_shape) == 4:
        batch_shape = [None] + img_shape[1:]
    else:
        batch_shape = [None] + img_shape

    transformer_kwargs = dict(input_shape=batch_shape,
                              num_classes=num_classes,
                              pretrain=args.pretrain,
                              deep_root=args.deep_root,
                              patch_size=args.patch_size,
                              use_fourier_embedding=args.use_fourier_embedding,
                              l2=0 if args.optimizer.lower() == "adamw" else args.l2)
    if args.model_name.lower() == "xcit_tst":
        model = nn.xcit_tst(**transformer_kwargs)
    elif args.model_name.lower() == "cait_xxs24":
        model = nn.cait_xxs24(**transformer_kwargs)
    elif args.model_name.lower() == "cait_xs36":
        model = nn.cait_xs36(**transformer_kwargs)
    elif args.model_name.lower() == "cait_s24":
        model = nn.cait_s24(**transformer_kwargs)
    elif args.model_name.lower() == "cait_m24":
        model = nn.cait_m24(**transformer_kwargs)
    elif args.model_name.lower() == "cait_s48":
        model = nn.cait_s48(**transformer_kwargs)
    elif args.model_name.lower() == "cait_s36":
        model = nn.cait_s36(**transformer_kwargs)
    elif args.model_name.lower() == "xcit_s24":
        model = nn.xcit_s24(**transformer_kwargs)
    elif args.model_name.lower() == "xcit_m24":
        model = nn.xcit_m24(**transformer_kwargs)
    elif args.model_name.lower() == "xcit_s12":
        model = nn.xcit_s12(**transformer_kwargs)
    else:
        raise LookupError("Please use a valid model. Your choice was: {} which is "
                          "not in the list of allowed models.".format(args.model_name.lower()))

    nt_str = "num_train_examples num_test_examples num_classes " \
             "total_steps batch_shape steps_per_epoch eval_steps_per_epoch"
    train_info_tuple = namedtuple("train_info_tuple",
                                  nt_str)

    train_info = train_info_tuple(num_train_examples=train_examples,
                                  num_test_examples=test_examples,
                                  num_classes=num_classes,
                                  total_steps=total_train_steps,
                                  batch_shape=batch_shape,
                                  steps_per_epoch=steps_per_epoch,
                                  eval_steps_per_epoch=eval_steps_per_epoch)

    return model, train_info


def get_metrics(pretrain=False):
    if pretrain:
        metric_list = ["total_loss", "regularization_loss", "contrast_loss",
                       "weighted_contrast_loss", "reconstruction_loss",
                       "weighted_reconstruction_loss", "rotational_loss",
                       "weighted_rotational_loss", "contrast_accuracy",
                       "contrast_entropy", "rotational_weight",
                       "contrastive_weight", "reconstructive_weight",
                       "rotational_accuracy"]
        metric_str = ""
        for a in ["pretrain_train", "pretrain_eval"]:
            base = "__{} ".format(a)
            metric_str += base.join(metric_list) + base
        metric_str = metric_str.replace("regularization_loss__pretrain_eval", "")
        metric_str = metric_str.replace("rotational_weight__pretrain_eval contrastive_"
                                        "weight__pretrain_eval reconstructive_weight__pretrain_eval", "")

        metrics_tuple = namedtuple("metrics_tuple", metric_str)

        # Train Losses
        total_loss_train = tfkm.Mean("total_loss_train")
        regularization_loss_train = tfkm.Mean("regularization_loss_train")
        contrast_loss_train = tfkm.Mean("contrast_loss_train")
        weighted_contrast_loss_train = tfkm.Mean("weighted_contrast_loss_train")
        reconstruction_loss_train = tfkm.Mean("reconstruction_loss_train")
        weighted_reconstruction_loss_train = tfkm.Mean("weighted_reconstruction_loss_train")
        rotational_loss_train = tfkm.Mean("rotational_loss_train")
        weighted_rotational_loss_train = tfkm.Mean("weighted_rotational_loss_train")

        # Train Metrics
        contrast_accuracy_train = tfkm.Mean("contrast_accuracy_train")
        contrast_entropy_train = tfkm.Mean("contrast_entropy_train")
        rotational_weight = tfkm.Mean("rotational_weight")
        contrastive_weight = tfkm.Mean("contrastive_weight")
        reconstructive_weight = tfkm.Mean("reconstructive_weight")
        rotational_accuracy = tfkm.Accuracy(name="rotational_accuracy")

        # Eval Losses
        total_loss_eval = tfkm.Mean("total_loss_eval")
        contrast_loss_eval = tfkm.Mean("contrast_loss_eval")
        weighted_contrast_loss_eval = tfkm.Mean("weighted_contrast_loss_eval")
        reconstruction_loss_eval = tfkm.Mean("reconstruction_loss_eval")
        weighted_reconstruction_loss_eval = tfkm.Mean("weighted_reconstruction_loss_eval")
        rotational_loss_eval = tfkm.Mean("rotational_loss_eval")
        weighted_rotational_loss_eval = tfkm.Mean("weighted_rotational_loss_eval")

        # Eval Metrics
        contrast_accuracy_eval = tfkm.Mean("contrast_accuracy_eval")
        contrast_entropy_eval = tfkm.Mean("contrast_entropy_eval")
        rotational_accuracy_eval = tfkm.Accuracy(name="rotational_accuracy_eval")

        eval_metrics = [total_loss_train, regularization_loss_train, contrast_loss_train,
                        weighted_contrast_loss_train, reconstruction_loss_train,
                        weighted_reconstruction_loss_train, rotational_loss_train,
                        weighted_rotational_loss_train, contrast_accuracy_train,
                        contrast_entropy_train, rotational_weight,
                        contrastive_weight, reconstructive_weight, rotational_accuracy,
                        total_loss_eval, contrast_loss_eval, weighted_contrast_loss_eval,
                        reconstruction_loss_eval, weighted_reconstruction_loss_eval,
                        rotational_loss_eval, weighted_rotational_loss_eval,
                        contrast_accuracy_eval, contrast_entropy_eval, rotational_accuracy_eval]
    else:
        metric_list = ["total_loss", "cross_entropy", "regularization_loss",
                       "tp", "fp", "tn", "fn", "accuracy", "precision", "recall", "top5"]

        metric_str = ""
        for a in ["train", "eval"]:
            base = "__{} ".format(a)
            metric_str += base.join(metric_list) + base
        metric_str = metric_str.replace("total_loss__eval", "")
        metric_str = metric_str.replace("regularization_loss__eval", "")

        metrics_tuple = namedtuple("metrics_tuple", metric_str)

        # Train losses
        total_loss_train = tfkm.Mean(name="total_loss_train")
        cross_entropy_train = tfkm.Mean(name="cross_entropy_train")
        regularization_loss_train = tfkm.Mean(name="regularization_loss_train")

        # Train Metrics
        tp_train = tfkm.TruePositives(name="tp_train")
        fp_train = tfkm.FalsePositives(name="fp_train")
        tn_train = tfkm.TrueNegatives(name="tn_train")
        fn_train = tfkm.FalseNegatives(name="fn_train")
        accuracy_train = tfkm.CategoricalAccuracy(name="accuracy_train")
        precision_train = tfkm.Precision(name="precision_train")
        recall_train = tfkm.Recall(name="recall_train")
        top5_train = tfkm.Mean(name="top5_train")

        # Eval Losses
        cross_entropy_eval = tfkm.Mean(name="cross_entropy_eval")

        # Eval Metrics
        tp_eval = tfkm.TruePositives(name="tp_eval")
        fp_eval = tfkm.FalsePositives(name="fp_eval")
        tn_eval = tfkm.TrueNegatives(name="tn_eval")
        fn_eval = tfkm.FalseNegatives(name="fn_eval")
        accuracy_eval = tfkm.CategoricalAccuracy(name="accuracy_eval")
        precision_eval = tfkm.Precision(name="precision_eval")
        recall_eval = tfkm.Recall(name="recall_eval")
        top5_eval = tfkm.Mean(name="top5_eval")

        eval_metrics = [total_loss_train, cross_entropy_train, regularization_loss_train,
                        tp_train, fp_train, tn_train, fn_train, accuracy_train, precision_train,
                        recall_train, top5_train, cross_entropy_eval, tp_eval, fp_eval,
                        tn_eval, fn_eval, accuracy_eval, precision_eval, recall_eval, top5_eval]

    keys = list(filter(None, metric_str.split(" ")))
    metrics = metrics_tuple(**dict(zip(keys, eval_metrics)))
    return metrics


def contrastive_loss(hidden1, hidden2,
                     args, use_cosine=False,
                     epsilon=1e9):
    """Compute loss for model.
    Args:
      hidden1: hidden vector 1 (`Tensor`) of shape (bsz, dim).
      hidden2: hidden vector 2 (`Tensor`) of shape (bsz, dim).
      args: The args dict
      use_cosine: If the cosine variant of this loss should be used
      epsilon: Large Number for numerical stability
    Returns:
      A loss scalar.
      The logits for contrastive prediction task.
      The labels for contrastive prediction task.
    """
    # Get (normalized) hidden1 and hidden2.
    bs = tf.shape(hidden1)[0]
    z_i = tf.math.l2_normalize(hidden1, 1)
    z_j = tf.math.l2_normalize(hidden2, 1)

    logits_ab = tf.matmul(z_i, tf.identity(z_j), transpose_b=True) / args.clr_temperature
    labels = tf.one_hot(tf.range(bs), bs * 2)

    if use_cosine:
        representations = tf.concat([z_i, z_j], 0)
        similarity_matrix = tf.losses.cosine_similarity(tf.expand_dims(representations, 1),
                                                        tf.expand_dims(representations, 0),
                                                        axis=2)

        sim_ij = tf.linalg.diag_part(similarity_matrix, k=bs)
        sim_ji = tf.linalg.diag_part(similarity_matrix, k=-bs)
        positives = tf.concat([sim_ij, sim_ji], 0)

        nominator = tf.math.exp(positives / args.clr_temperature)
        negatives_mask = tf.cast(tf.math.logical_not(tf.linalg.eye(bs * 2, bs * 2, dtype=tf.bool)), tf.float32)
        denominator = negatives_mask * tf.math.exp(similarity_matrix / args.clr_temperature)

        losses = -.5 * tf.math.log(nominator / tf.reduce_sum(denominator, axis=1))  # [2 * bs, 1]
    else:
        z_i_large = tf.identity(z_i)
        z_j_large = tf.identity(z_j)

        masks = tf.one_hot(tf.range(bs), bs)

        logits_aa = tf.matmul(z_i, z_i_large, transpose_b=True) / args.clr_temperature
        logits_aa = logits_aa - masks * epsilon

        logits_bb = tf.matmul(z_j, z_j_large, transpose_b=True) / args.clr_temperature
        logits_bb = logits_bb - masks * epsilon
        logits_ba = tf.matmul(z_j, z_i_large, transpose_b=True) / args.clr_temperature

        loss_a = tf.nn.softmax_cross_entropy_with_logits(
            labels, tf.concat([logits_ab, logits_aa], 1))
        loss_b = tf.nn.softmax_cross_entropy_with_logits(
            labels, tf.concat([logits_ba, logits_bb], 1))

        losses = loss_a + loss_b  # [bs, 1]

    # Accuracy
    con_acc = tf.equal(tf.math.argmax(logits_ab, axis=-1),
                       tf.math.argmax(labels, axis=-1))

    # Return also the logits, for entropy calculation
    return losses, con_acc, logits_ab


def cross_entropy(labels, logits, args):
    """Compute mean supervised loss over local batch."""
    losses = tf.keras.losses.CategoricalCrossentropy(
        reduction=tf.keras.losses.Reduction.NONE,
        from_logits=True)(y_true=labels,
                          y_pred=logits)  # [2 * bs, 1]
    losses = tf.reduce_sum(losses) * (1. / (2. * args.batch_size))
    return losses


def mae(labels, preds, args):
    """Compute mean supervised loss over local batch."""
    losses = tf.keras.losses.MeanAbsoluteError(
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs, height, width, channel]

    # Compute loss that is scaled by sample_weight and by global batch size.
    losses = tf.reduce_sum(losses) * (1. / (2. * args.batch_size))
    return losses


# noinspection PyProtectedMember
class AdamWeightDecay(tf.keras.optimizers.Adam):
    """
    Adam enables L2 weight decay and clip_by_global_norm on gradients. Just adding the square of the weights to the
    loss function is *not* the correct way of using L2 regularization/weight decay with Adam, since that will interact
    with the m and v parameters in strange ways as shown in `Decoupled Weight Decay Regularization
    <https://arxiv.org/abs/1711.05101>`__.

    Instead we want ot decay the weights in a manner that doesn't interact with the m/v parameters. This is equivalent
    to adding the square of the weights to the loss with plain (non-momentum) SGD.

    Args:
        learning_rate (:obj:`Union[float, tf.keras.optimizers.schedules.LearningRateSchedule]`, `optional`,
            defaults to 1e-3): The learning rate to use or a schedule.
        beta_1 (:obj:`float`, `optional`, defaults to 0.9):
            The beta1 parameter in Adam, which is the exponential decay rate for the 1st momentum estimates.
        beta_2 (:obj:`float`, `optional`, defaults to 0.999):
            The beta2 parameter in Adam, which is the exponential decay rate for the 2nd momentum estimates.
        epsilon (:obj:`float`, `optional`, defaults to 1e-7):
            The epsilon parameter in Adam, which is a small constant for numerical stability.
        amsgrad (:obj:`bool`, `optional`, default to `False`):
            Whether to apply AMSGrad variant of this algorithm or not, see `On the Convergence of Adam and Beyond
            <https://arxiv.org/abs/1904.09237>`__.
        weight_decay_rate (:obj:`float`, `optional`, defaults to 0):
            The weight decay to apply.
        include_in_weight_decay (:obj:`List[str]`, `optional`):
            List of the parameter names (or re patterns) to apply weight decay to. If none is passed, weight decay is
            applied to all parameters by default (unless they are in :obj:`exclude_from_weight_decay`).
        exclude_from_weight_decay (:obj:`List[str]`, `optional`):
            List of the parameter names (or re patterns) to exclude from applying weight decay to. If a
            :obj:`include_in_weight_decay` is passed, the names in it will supersede this list.
        name (:obj:`str`, `optional`, defaults to 'AdamWeightDecay'):
            Optional name for the operations created when applying gradients.
        kwargs:
            Keyword arguments. Allowed to be {``clipnorm``, ``clipvalue``, ``lr``, ``decay``}. ``clipnorm`` is clip
            gradients by norm; ``clipvalue`` is clip gradients by value, ``decay`` is included for backward
            compatibility to allow time inverse decay of learning rate. ``lr`` is included for backward compatibility,
            recommended to use ``learning_rate`` instead.
    """

    def __init__(
            self,
            learning_rate=0.001,
            beta_1: float = 0.9,
            beta_2: float = 0.999,
            epsilon: float = 1e-7,
            amsgrad: bool = False,
            weight_decay_rate: float = 0.0,
            include_in_weight_decay=None,
            exclude_from_weight_decay=None,
            name: str = "AdamWeightDecay",
            **kwargs
    ):
        super().__init__(learning_rate, beta_1, beta_2, epsilon, amsgrad, name, **kwargs)
        self.weight_decay_rate = weight_decay_rate
        self._include_in_weight_decay = include_in_weight_decay
        self._exclude_from_weight_decay = exclude_from_weight_decay

    @classmethod
    def from_config(cls, config, **kwargs):
        """Creates an optimizer from its config with WarmUp custom object.
        """
        return super(AdamWeightDecay, cls).from_config(config)

    def _prepare_local(self, var_device, var_dtype, apply_state):
        super(AdamWeightDecay, self)._prepare_local(var_device, var_dtype, apply_state)
        apply_state[(var_device, var_dtype)]["weight_decay_rate"] = tf.constant(
            self.weight_decay_rate, name="adam_weight_decay_rate"
        )

    def _decay_weights_op(self, var, learning_rate, apply_state):
        do_decay = self._do_use_weight_decay(var.name)
        if do_decay:
            return var.assign_sub(
                learning_rate * var * apply_state[(var.device, var.dtype.base_dtype)]["weight_decay_rate"],
                use_locking=self._use_locking,
            )
        return tf.no_op()

    def apply_gradients(self, grads_and_vars, name=None, **kwargs):
        grads, tvars = list(zip(*grads_and_vars))
        return super(AdamWeightDecay, self).apply_gradients(zip(grads, tvars), name=name, **kwargs)

    def _get_lr(self, var_device, var_dtype, apply_state):
        """Retrieves the learning rate with the given state."""
        if apply_state is None:
            return self._decayed_lr_t[var_dtype], {}

        apply_state = apply_state or {}
        coefficients = apply_state.get((var_device, var_dtype))
        if coefficients is None:
            coefficients = self._fallback_apply_state(var_device, var_dtype)
            apply_state[(var_device, var_dtype)] = coefficients

        return coefficients["lr_t"], dict(apply_state=apply_state)

    def _resource_apply_dense(self, grad, var, apply_state=None):
        lr_t, kwargs = self._get_lr(var.device, var.dtype.base_dtype, apply_state)
        decay = self._decay_weights_op(var, lr_t, apply_state)
        with tf.control_dependencies([decay]):
            return super(AdamWeightDecay, self)._resource_apply_dense(grad, var, **kwargs)

    def _resource_apply_sparse(self, grad, var, indices, apply_state=None):
        lr_t, kwargs = self._get_lr(var.device, var.dtype.base_dtype, apply_state)
        decay = self._decay_weights_op(var, lr_t, apply_state)
        with tf.control_dependencies([decay]):
            return super(AdamWeightDecay, self)._resource_apply_sparse(grad, var, indices, **kwargs)

    def get_config(self):
        config = super().get_config()
        config.update({"weight_decay_rate": self.weight_decay_rate})
        return config

    def _do_use_weight_decay(self, param_name):
        """Whether to use L2 weight decay for `param_name`."""
        if self.weight_decay_rate == 0:
            return False

        if self._include_in_weight_decay:
            for r in self._include_in_weight_decay:
                if re.search(r, param_name) is not None:
                    return True

        if self._exclude_from_weight_decay:
            for r in self._exclude_from_weight_decay:
                if re.search(r, param_name) is not None:
                    return False
        return True


def accuracy(output, target, topk=(1,)):
    """
    Computes the accuracy over the k top predictions
    for the specified values of k.
    This is based on:
    https://github.com/rwightman/pytorch-image-models/blob/master/timm/utils/metrics.py
    """
    maxk = max(topk)
    shape = target.shape
    batch_size = shape[0]

    if batch_size != shape[-1]:  # Make Sparse labels
        target = tf.argmax(target, -1, output_type=tf.int32)

    _, indices = tf.math.top_k(output, k=maxk, sorted=True)
    pred = tf.transpose(indices)
    correct = tf.math.equal(pred, target)
    out = [tf.reduce_sum(
        tf.cast(tf.reshape(correct[:k], [batch_size, -1]),
                tf.float32)) * 100. / batch_size for k in topk]
    return out[0] if len(topk) == 1 else out
