import os
import subprocess
from nn import get_args, check_folder


def get_args_string(args_namespace):
    # Get the args base string
    args_str = ""
    for a in args_namespace.__dict__:
        var = a
        val = "{}".format(args_namespace.__dict__[a])
        if var == "split_list":
            val = val.replace("[", "").replace("]", "")
            val = val.replace(",", "")
            i = 1
            while True:
                if "'" in val:
                    val = val.replace("'", "")
                else:
                    break

        args_str += "--{}={}|".format(var, val)

    return args_str


if __name__ == "__main__":
    args = get_args()
    args_str = get_args_string(args)

    if "pretrain" in args.train_mode:

        check_folder(args.work_dir, False)

        redirect_str = " > {} 2> {} <&- &".format(os.path.join(args.work_dir, "info_out"),
                                                  os.path.join(args.work_dir, "log_out"))
        base_str = "nohup|python|pretrain.py|" + args_str + redirect_str

        # Run the system command
        result = subprocess.run(base_str.split("|"),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        if result.returncode == 0 and "finetune" in args.train_mode:
            args.base_model_dir = args.work_dir
