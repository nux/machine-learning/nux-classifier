"""
### TF2 implementation of a Self-Supervised Class-Attention Cross-Covariance Visual Transformer ###

The TF2 implementations of the following papers:

1. Dosovitskiy, A. et al. An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale. (2020).
2. Shazeer, N., Lan, Z., Cheng, Y., Ding, N. & Hou, L. Talking-Heads Attention. (2020).
3. Touvron, H., Cord, M., Sablayrolles, A., Synnaeve, G. & Jégou, H. Going deeper with Image Transformers. (2021).
4. Atito, S., Awais, M. & Kittler, J. SiT: Self-supervised vIsion Transformer. (2021).
5. El-Nouby, A. et al. XCiT: Cross-Covariance Image Transformers. (2021).
6. Xiao, T. et al. Early Convolutions Help Transformers See Better. (2021).

Author: jzimmermann@phys.ethz.ch

    For fine-tuning:
    From: Chen, T. et al. A Simple Framework for Contrastive Learning of Visual Representations,
    and Chen, T. et al. Big Self-Supervised Models are Strong Semi-Supervised Learners:

    A deeper projection head not only improves the representation quality
    measured by linear evaluation, but also improves semi-supervised performance
    when fine-tuning from a middle layer of the projection head.

    Fine-tuning is a common way to adapt the task-agnostically pretrained network
    for a specific task. In SimCLR [1], the MLP projection head g(·) is discarded
    entirely after pretraining, while only the ResNet encoder f(·) is used during
    the fine-tuning. Instead of throwing it all away, we propose to incorporate part
    of the MLP projection head into the base encoder during the fine-tuning. In
    other words, we fine-tune the model from a middle layer of the projection head,
    instead of the input layer of the projection head as in SimCLR. Note that
    fine-tuning from the first layer of the MLP head is the same as adding an
    fully-connected layer to the base network and removing an fully-connected
    layer from the head, and the impact of this extra layer is contingent on the
    amount of labeled examples during fine-tuning (as shown in our experiments).

    For fine-tuning, by default we fine-tune from the first layer of the projection
    head for 1%/10% of labeled examples, but from the input of the projection head
    when 100% labels are present. We use global batch normalization, but we remove
    weight decay, learning rate warmup, and use a much smaller learning rate,
    i.e. 0.16 (= 0.005 × sqrt(BatchSize)) for standard ResNets [25], and
    0.064 (= 0.002 × sqrt(BatchSize)) for larger ResNets variants (with width
    multiplier larger than 1 and/or SK [28]). A batch size of 1024 is used.
    Similar to [1], we fine-tune for 60 epochs with 1% of labels, and 30 epochs
    with 10% of labels, as well as full ImageNet labels.
"""
import os
import nn
import json
import socket
import resource

low, high = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (high, high))
import logging
from tqdm import tqdm
import tensorflow as tf

tfk = tf.keras
tfkl = tfk.layers

# GLOBAL VARIABLES & PERFORMANCE STUFF
# setting GPU order, CPU opts, and enabling XLA JIT compiler
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["OMP_NUM_THREADS"] = str(os.cpu_count())
os.environ["KMP_BLOCKTIME"] = "30"
os.environ["KMP_SETTINGS"] = "1"
os.environ["KMP_AFFINITY"] = "granularity=fine,verbose,compact,1,0"

if socket.gethostname() == "nux-noether":
    # os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
    os.environ["XLA_FLAGS"] = "--xla_gpu_cuda_data_dir=/usr/local/cuda-11/"
os.environ["TF_XLA_FLAGS"] = "--tf_xla_auto_jit=2 " \
                             "--tf_xla_cpu_global_jit " \
                             "--tf_xla_enable_xla_devices"


# noinspection PyProtectedMember,PyUnboundLocalVariable
def finetune(args):
    @tf.function
    def train_step(dist_input):
        def forward_pass():
            with tf.GradientTape() as tape:
                rot_p, contrastive_p = model(image_batch, training=True)
                cross_entropy_1 = nn.cross_entropy(labels=label_batch, logits=rot_p, args=args)
                cross_entropy_2 = nn.cross_entropy(labels=label_batch, logits=contrastive_p, args=args)
                cross_entropy = cross_entropy_1 + cross_entropy_2

                # If Adamw is used, this will always be zero
                reg_loss_unscaled = tf.reduce_sum(tf.cast(model.losses, tf.float32))
                regularization_loss = tf.nn.scale_regularization_loss(reg_loss_unscaled)

                loss = cross_entropy + regularization_loss
                if args.fp16:
                    loss = optimizer.get_scaled_loss(loss)

            grads = tape.gradient(loss, model.trainable_variables)
            if args.fp16:
                grads = optimizer.get_unscaled_gradients(grads)

            return_args = [loss, cross_entropy, regularization_loss, label_batch, rot_p, contrastive_p]
            return grads, return_args

        image_batch, label_batch = dist_input[0], dist_input[1]

        if "helium" not in args.dataset and not args.use_mixup:
            # most datasets are sparsely labelled
            label_batch = tf.one_hot(label_batch, depth=train_info.num_classes, axis=-1)

        gradients, ret_arr = forward_pass()

        if args.sam:
            # Sharpness-aware minimization:
            # Foret, P., Kleiner, A., Mobahi, H. & Neyshabur, B.
            # Sharpness-Aware Minimization for Efficiently Improving Generalization. (2020).
            e_ws = []
            grad_norm = tf.linalg.global_norm(gradients)
            for i_var in range(len(model.trainable_variables)):
                e_w = gradients[i_var] * args.sam_rho / (grad_norm + 1e-7)
                model.trainable_variables[i_var].assign_add(e_w)
                e_ws.append(e_w)

            gradients, ret_arr = forward_pass()

            for i_var in range(len(model.trainable_variables)):
                model.trainable_variables[i_var].assign_add(-e_ws[i_var])

        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        return ret_arr

    @tf.function
    def eval_step(dist_input):
        image_batch, label_batch = dist_input[0], dist_input[1]

        if "helium" not in args.dataset:  # most datasets are sparsely labelled
            label_batch = tf.one_hot(label_batch, depth=train_info.num_classes, axis=-1)

        rot_p, contrastive_p = model(image_batch, training=False)
        cross_entropy_1 = nn.cross_entropy(labels=label_batch, logits=rot_p, args=args)
        cross_entropy_2 = nn.cross_entropy(labels=label_batch, logits=contrastive_p, args=args)
        cross_entropy = cross_entropy_1 + cross_entropy_2

        if args.fp16:
            cross_entropy = optimizer.get_scaled_loss(cross_entropy)

        return cross_entropy, label_batch, rot_p, contrastive_p

    # noinspection PyProtectedMember
    @tf.function
    def distributed_train_step(dist_inputs):
        _arr = strategy.run(train_step, args=(dist_inputs,))
        loss, cross_entropy, reg_loss, lbl, rot_p, contrastive_p = _arr

        if len(args.gpus) > 1:
            lbl = tf.concat(lbl.values, 0)
            rot_p = tf.concat(rot_p.values, 0)
            contrastive_p = tf.concat(contrastive_p.values, 0)
        predictions = (tf.nn.softmax(rot_p) + tf.nn.softmax(contrastive_p)) / 2

        loss = strategy.reduce("SUM", loss, axis=None)
        ce_loss = strategy.reduce("SUM", cross_entropy, axis=None)
        reg_loss = strategy.reduce("SUM", reg_loss, axis=None)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "total_loss__train":
                val_.update_state(values=loss)
            if key_ == "cross_entropy__train":
                val_.update_state(values=ce_loss)
            if key_ == "regularization_loss__train":
                val_.update_state(values=reg_loss)
            if "train" in key_ and "loss" not in key_ and "cross_entropy" not in key_:
                if "top5" in key_:
                    val_.update_state(values=nn.accuracy(predictions, lbl, (5,)))
                else:
                    val_.update_state(y_true=lbl, y_pred=predictions)
        return predictions

    # noinspection PyProtectedMember
    @tf.function
    def distributed_eval_step(dist_inputs):
        _arr = strategy.run(eval_step, args=(dist_inputs,))
        cross_entropy, lbl, rot_p, contrastive_p = _arr

        if len(args.gpus) > 1:
            lbl = tf.concat(lbl.values, 0)
            rot_p = tf.concat(rot_p.values, 0)
            contrastive_p = tf.concat(contrastive_p.values, 0)
        predictions = (tf.nn.softmax(rot_p) + tf.nn.softmax(contrastive_p)) / 2

        ce_loss = strategy.reduce("SUM", cross_entropy, axis=None)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "cross_entropy__eval":
                val_.update_state(values=ce_loss)
            if "eval" in key_ and "cross_entropy" not in key_:
                if "top5" in key_:
                    val_.update_state(values=nn.accuracy(predictions, lbl, (5,)))
                else:
                    val_.update_state(y_true=lbl, y_pred=predictions)

        return predictions

    if args.fp16:
        # we use mixed FP16 precision
        # With this policy, layers use float16 computations and float32 variables.
        # Computations are done in float16 for performance, but variables must be kept in
        # float32 for numeric stability.
        policy = tfk.mixed_precision.Policy('mixed_float16')
        tfk.mixed_precision.set_global_policy(policy)

    # Set up Multi-GPU
    strategy = tf.distribute.MirroredStrategy(devices=args.gpus)

    # Get Data
    (test, train), info, train_examples, test_examples = nn.get_data(args.dataset, args=args)
    test = strategy.experimental_distribute_dataset(test)
    train = strategy.experimental_distribute_dataset(train)

    args.per_device_batch_size = int(args.batch_size / strategy.num_replicas_in_sync)

    # Save the full config:
    with open(os.path.join(args.paths.log_dir, "config.txt"), 'w') as f_conf:
        json.dump(nn.without_keys(args.__dict__, ["logger"]), f_conf, indent=2)

    # Build metrics
    metrics = nn.get_metrics(pretrain=False)

    # Get model
    with strategy.scope():
        model, train_info = nn.get_model(args, info)
        # Setting up tensorboard
        summary_writer = tf.summary.create_file_writer(args.paths.log_dir)
        if args.base_model_epoch:
            epoch = args.base_model_epoch
            load_str = "clr_epoch-{}".format(epoch)
        else:
            path_ = tf.train.latest_checkpoint(os.path.join(args.base_model_dir, "weights"))
            if path_:
                load_str = os.path.basename(path_)
            else:
                args.logger.error("Please provide a valid base-line model")
                raise ValueError

        args.logger.info("\n\n\t[!] Loading the weights from: {}\n".format(load_str))
        restore_path = os.path.join(args.base_model_dir, "weights", load_str)
        model.build(input_shape=(None, args.img_size, args.img_size, args.img_channel))
        # Needed for correctly displaying the Output shapes in summary
        # see: https://stackoverflow.com/a/65956968/4550763
        model.call(tfkl.Input(shape=(args.img_size, args.img_size, args.img_channel)))

        load_status = model.load_weights(restore_path).expect_partial()
        load_status.assert_nontrivial_match()
        # Print out model summary:
        model.summary(print_fn=args.logger.info)

        # Define optimizer and learning rate schedule
        warm_up = int(5 * train_info.steps_per_epoch)  # Warmup for 5 epochs
        if args.lr == 0.:
            init_lr = 5e-4 * args.batch_size / 512  # See Table 9 in arXiv:2012.12877 [cs.CV]
        else:
            init_lr = args.lr
        # Set the cosine learning rate scheduler, see arXiv:1812.01187 [cs.CV]
        learning_rate = nn.LinearCosineSchedule(initial_learning_rate=init_lr,
                                                warmup_steps=warm_up,
                                                total_steps=train_info.total_steps)

        # Optimizer
        if args.optimizer.lower() == "sgd":
            optimizer = tfk.optimizers.SGD(learning_rate=learning_rate,
                                           momentum=.9,
                                           nesterov=False)
        elif args.optimizer.lower() == "adam":
            optimizer = tfk.optimizers.Adam(learning_rate=learning_rate,
                                            beta_1=0.9, beta_2=0.999)
        elif args.optimizer.lower() == "adamw":
            # We use the same scheduler for the weight_decay as for the learning_rate
            optimizer = nn.AdamWeightDecay(learning_rate=learning_rate,
                                           weight_decay_rate=args.l2,
                                           beta_1=0.9, beta_2=0.999)
        else:
            raise LookupError("Optimizer should either be 'sgd', 'adamw' or 'adam'")

        if args.fp16:
            # use a dynamic loss scale, see https://www.tensorflow.org/guide/mixed_precision
            optimizer = tfk.mixed_precision.LossScaleOptimizer(optimizer, dynamic=True,
                                                               initial_scale=1 if args.sam else None)

    # start training
    step = 0
    for epoch in range(args.epochs):
        # Eval first
        if epoch % args.test_every_n_epoch == 0:
            it_test = tqdm(test, total=train_info.eval_steps_per_epoch)
            for features in it_test:
                eval_images, eval_labels = features[0], features[1]
                eval_preds = distributed_eval_step([eval_images, eval_labels])

                args_tmp = (epoch + 1, args.epochs,
                            metrics.cross_entropy__eval.result().numpy(),
                            metrics.accuracy__eval.result().numpy(),
                            metrics.precision__eval.result().numpy(),
                            metrics.recall__eval.result().numpy())
                st = "Eval || Epoch: {}/{}, Loss: {:.5f}, Accuracy: {:.5f}, " \
                     "Precision: {:.5f}, Recall: {:.5f}".format(*args_tmp)
                it_test.set_postfix_str(st)
            args.logger.info(st)

            if summary_writer is not None:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        eval_images = tf.concat(eval_images.values, 0)
                        eval_labels = tf.concat(eval_labels.values, 0)

                    if "helium" not in args.dataset:  # most datasets are sparsely labelled
                        eval_labels = tf.one_hot(eval_labels, depth=train_info.num_classes, axis=-1)
                    eval_pred_image = nn.build_lbl_pred_png(eval_labels,
                                                            tf.round(eval_preds),
                                                            eval_preds)

                    tf.summary.image("Test Images",
                                     nn.normalize_img(eval_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Prediction Image Test", eval_pred_image, step=step)

                    tf.summary.histogram("Test/Images",
                                         tf.reshape(eval_images, [-1]),
                                         step=step)
                    for k, v in zip(metrics._fields, metrics):
                        if "eval" in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=step)

        it_train = tqdm(train, total=train_info.steps_per_epoch)
        for features in it_train:
            step += 1
            # images have 4*channels as two samples are augmented per step and
            # both samples are distorted individually. They are then stacked
            # along the channels dim
            images, labels = features[0], features[1]
            preds = distributed_train_step([images, labels])

            if step % args.log_steps == 0 or step == 1:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        images = tf.concat(images.values, 0)
                        labels = tf.concat(labels.values, 0)

                    if "helium" not in args.dataset and not args.use_mixup:
                        # most datasets are sparsely labelled
                        labels = tf.one_hot(labels, depth=train_info.num_classes, axis=-1)
                    pred_image = nn.build_lbl_pred_png(labels, tf.round(preds), preds)

                    tf.summary.image("Training Images",
                                     nn.normalize_img(images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Prediction Image Train", pred_image, step=step)

                    tf.summary.histogram("Train/Images",
                                         tf.reshape(images, [-1]),
                                         step=step)

                    tf.summary.scalar("learning_rate", learning_rate(float(step)), step=step)
                    for k, v in zip(metrics._fields, metrics):
                        if "_train" in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=step)

            args_tmp = (epoch + 1, args.epochs, step, train_info.total_steps,
                        metrics.total_loss__train.result().numpy(),
                        metrics.accuracy__train.result().numpy(),
                        metrics.precision__train.result().numpy(),
                        metrics.recall__train.result().numpy())
            st = "Train || Epoch: {}/{}, Step: {}/{}, Loss: {:.5f}, Accuracy: {:.5f}, " \
                 "Precision: {:.5f}, Recall: {:.5f}".format(*args_tmp)
            it_train.set_postfix_str(st)
        args.logger.info(st)

        for m in metrics:
            m.reset_states()

        if epoch % args.save_every_n_epoch == 0:
            model.save_weights(filepath=os.path.join(args.paths.weight_dir, "finetune_epoch-{}".format(epoch)),
                               save_format="tf")

    # save model after training is completed
    save_str = "finetune_model_final_after_epoch-{}".format(args.epochs)
    model.save_weights(filepath=os.path.join(args.paths.weight_dir, save_str), save_format="tf")


def main(args):
    # parse arguments
    if args is None:
        exit()

    # Build the model train it
    finetune(args)


if __name__ == "__main__":
    arguments = nn.get_args()
    arguments.pretrain = False
    arguments.paths = nn.set_path_env(arguments, False)

    # For logging:
    arguments.logger = tf.get_logger()
    arguments.logger.setLevel(logging.INFO)
    arguments.logger.propagate = False

    # Load the config of the pretrained model
    pretrain_conf = os.path.join(arguments.base_model_dir, "logs", "config.txt")
    if not os.path.isfile(pretrain_conf):
        raise FileNotFoundError("Config file not found at: {}".format(pretrain_conf))

    with open(pretrain_conf, 'r') as f:
        arguments_pretrain = json.load(f)
    # From the pretrain conf we only need the model name
    arguments.model_name = arguments_pretrain["model_name"]

    # Set up all paths
    arguments.paths = nn.set_path_env(arguments, True)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(arguments.paths.log_dir, "train.log"))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    arguments.logger.addHandler(fh)

    if not arguments.gpus:
        arguments.logger.info("A supported GPU is necessary")
        exit()
    else:
        tf.config.set_soft_device_placement(True)
        physical_devices = tf.config.list_physical_devices('GPU')
        tf.config.threading.set_inter_op_parallelism_threads(2)
        tf.config.threading.set_intra_op_parallelism_threads(os.cpu_count())
        # noinspection PyBroadException
        try:
            for i, g in enumerate(physical_devices):
                if i < 4:  # GPU with index 4 is the student card on our machine
                    tf.config.experimental.set_memory_growth(g, True)
        except:
            # Invalid device or cannot modify virtual devices once initialized.
            pass
        main(arguments)

        # close the handlers
        for handler in arguments.logger.handlers:
            handler.close()
            arguments.logger.removeFilter(handler)
