"""
### TF2 implementation of a Self-Supervised Class-Attention Cross-Covariance Visual Transformer ###

The TF2 implementations of the following papers:

1. Dosovitskiy, A. et al. An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale. (2020).
2. Shazeer, N., Lan, Z., Cheng, Y., Ding, N. & Hou, L. Talking-Heads Attention. (2020).
3. Touvron, H., Cord, M., Sablayrolles, A., Synnaeve, G. & Jégou, H. Going deeper with Image Transformers. (2021).
4. Atito, S., Awais, M. & Kittler, J. SiT: Self-supervised vIsion Transformer. (2021).
5. El-Nouby, A. et al. XCiT: Cross-Covariance Image Transformers. (2021).
6. Xiao, T. et al. Early Convolutions Help Transformers See Better. (2021).

Author: jzimmermann@phys.ethz.ch

"""
import os
import nn
import json
import socket
import resource

low, high = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (high, high))
import logging
from tqdm import tqdm
import tensorflow as tf

tfk = tf.keras
tfkl = tfk.layers

# GLOBAL VARIABLES & PERFORMANCE STUFF
# setting GPU order, CPU opts, and enabling XLA JIT compiler
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["OMP_NUM_THREADS"] = str(os.cpu_count())
os.environ["KMP_BLOCKTIME"] = "30"
os.environ["KMP_SETTINGS"] = "1"
os.environ["KMP_AFFINITY"] = "granularity=fine,verbose,compact,1,0"

if socket.gethostname() == "nux-noether":
    # os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
    os.environ["XLA_FLAGS"] = "--xla_gpu_cuda_data_dir=/usr/local/cuda-11/"
os.environ["TF_XLA_FLAGS"] = "--tf_xla_auto_jit=2 " \
                             "--tf_xla_cpu_global_jit " \
                             "--tf_xla_enable_xla_devices"


# noinspection PyUnboundLocalVariable,PyProtectedMember
def pre_train(args):
    @tf.function
    def train_step(dist_input):
        def forward_pass():
            with tf.GradientTape() as grad_tape:

                rot_p_1, contrastive_p_1, imgs_recon_1, r_w, cn_w, rec_w = model(distorted_features_1,
                                                                                 training=True)
                rot_p_2, contrastive_p_2, imgs_recon_2, _, _, _ = model(distorted_features_2,
                                                                        training=True)

                # Unweighted Losses
                rot_logits = tf.concat([rot_p_1, rot_p_2], 0)
                cross_entropy = nn.cross_entropy(labels=rot_label,
                                                 logits=rot_logits,
                                                 args=args)

                undistorted_imgs = tf.concat([undistorted_features_1,
                                              undistorted_features_2], 0)
                imgs_recon = tf.concat([imgs_recon_1,
                                        imgs_recon_2], 0)
                mae = nn.mae(undistorted_imgs, imgs_recon, args)

                con_loss, con_acc, con_log = nn.contrastive_loss(
                    hidden1=contrastive_p_1, hidden2=contrastive_p_2,
                    args=args, use_cosine=False)

                # Weighted Losses, see Cipolla, R.; et al. doi:10.1109/CVPR.2018.00781
                # and Atito, S; e al. arXiv:2104.03602 [cs.CV]
                # Softmax Out - Gaussian Likelihood -> 1/sigma^2 L + log(sigma)
                weighted_rot_loss = tf.math.exp(-r_w) * cross_entropy + 0.5 * r_w
                # L1 Regression - Laplacian Likelihood -> 1/sigma L + log(sigma)
                weighted_rec_loss = tf.math.exp(-0.5 * rec_w) * mae + 0.5 * rec_w
                # Softmax Out - Gaussian Likelihood -> 1/sigma^2 L + log(sigma)
                weighted_con_loss = tf.math.exp(-cn_w) * con_loss + 0.5 * cn_w

                # train_loss = cross_entropy
                train_loss = weighted_rot_loss + weighted_con_loss + weighted_rec_loss

                # If Adamw is used, this will always be zero
                reg_loss_unscaled = tf.reduce_sum(tf.cast(model.losses, tf.float32))
                regularization_loss = tf.nn.scale_regularization_loss(reg_loss_unscaled)

                loss_obj = train_loss + regularization_loss

                if args.fp16:
                    loss_obj = optimizer.get_scaled_loss(loss_obj)

            if args.fp16:
                scaled_grads = grad_tape.gradient(loss_obj, model.trainable_variables)
                grads = optimizer.get_unscaled_gradients(scaled_grads)
            else:
                grads = grad_tape.gradient(loss_obj, model.trainable_variables)

            return_args = [regularization_loss, loss_obj, con_loss, con_acc, con_log, cross_entropy,
                           weighted_rot_loss, mae, weighted_rec_loss, weighted_con_loss,
                           imgs_recon, r_w, cn_w, rec_w, rot_label, rot_logits]
            return grads, return_args

        img_, rot_label = dist_input[0], dist_input[1]  # [bs, h, w, 2*c], [bs, ]

        # Sampling
        # Split up the 12 channel samples into 6 channel ones
        [undistorted_img_pairs, distorted_img_pairs] = tf.split(
            img_, num_or_size_splits=2, axis=-1)

        # Split the 6 channel samples to their intended 3 channel ones
        (undistorted_features_1, undistorted_features_2) = tf.split(
            undistorted_img_pairs, num_or_size_splits=2, axis=-1)
        (distorted_features_1, distorted_features_2) = tf.split(
            distorted_img_pairs, num_or_size_splits=2, axis=-1)

        gradients, ret_arr = forward_pass()

        if args.sam:
            # Sharpness-aware minimization:
            # Foret, P., Kleiner, A., Mobahi, H. & Neyshabur, B.
            # Sharpness-Aware Minimization for Efficiently Improving Generalization. (2020).
            e_ws = []
            grad_norm = tf.linalg.global_norm(gradients)
            for i_var in range(len(model.trainable_variables)):
                e_w = gradients[i_var] * args.sam_rho / (grad_norm + 1e-7)
                model.trainable_variables[i_var].assign_add(e_w)
                e_ws.append(e_w)

            gradients, ret_arr = forward_pass()

            for i_var in range(len(model.trainable_variables)):
                model.trainable_variables[i_var].assign_add(-e_ws[i_var])

        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        return ret_arr

    @tf.function
    def eval_step(dist_input):
        img_, rot_label = dist_input[0], dist_input[1]  # [bs, h, w, 2*c], [bs, ]
        # Sampling
        # Split up the 12 channel samples into 6 channel ones
        [undistorted_img_pairs, distorted_img_pairs] = tf.split(
            img_, num_or_size_splits=2, axis=-1)

        # Split the 6 channel samples to their intended 3 channel ones
        (undistorted_features_1, undistorted_features_2) = tf.split(
            undistorted_img_pairs, num_or_size_splits=2, axis=-1)
        (distorted_features_1, distorted_features_2) = tf.split(
            distorted_img_pairs, num_or_size_splits=2, axis=-1)

        rot_p_1, contrastive_p_1, imgs_recon_1, r_w, cn_w, rec_w = model(distorted_features_1,
                                                                         training=False)
        rot_p_2, contrastive_p_2, imgs_recon_2, _, _, _ = model(distorted_features_2,
                                                                training=False)

        # Unweighted Losses
        rot_logits = tf.concat([rot_p_1, rot_p_2], 0)
        cross_entropy = nn.cross_entropy(labels=rot_label,
                                         logits=rot_logits,
                                         args=args)

        undistorted_imgs = tf.concat([undistorted_features_1,
                                      undistorted_features_2], 0)
        imgs_recon = tf.concat([imgs_recon_1,
                                imgs_recon_2], 0)
        mae = nn.mae(undistorted_imgs, imgs_recon, args)

        con_loss, con_acc, con_log = nn.contrastive_loss(
            hidden1=contrastive_p_1, hidden2=contrastive_p_2,
            args=args, use_cosine=False)

        # Weighted Losses, see Cipolla, R.; et al. doi:10.1109/CVPR.2018.00781
        # and Atito, S; e al. arXiv:2104.03602 [cs.CV]
        # Softmax Out - Gaussian Likelihood -> 1/sigma^2 L + log(sigma)
        weighted_rot_loss = tf.math.exp(-r_w) * cross_entropy + 0.5 * r_w
        # L1 Regression - Laplacian Likelihood -> 1/sigma L + log(sigma)
        weighted_rec_loss = tf.math.exp(-0.5 * rec_w) * mae + 0.5 * rec_w
        # Softmax Out - Gaussian Likelihood -> 1/sigma^2 L + log(sigma)
        weighted_con_loss = tf.math.exp(-cn_w) * con_loss + 0.5 * cn_w

        # train_loss = cross_entropy
        train_loss = weighted_rot_loss + weighted_con_loss + weighted_rec_loss

        # If Adamw is used, this will always be zero
        reg_loss_unscaled = tf.reduce_sum(tf.cast(model.losses, tf.float32))
        regularization_loss = tf.nn.scale_regularization_loss(reg_loss_unscaled)

        loss = train_loss + regularization_loss

        if args.fp16:
            loss = optimizer.get_scaled_loss(loss)

        ret_arr = [regularization_loss, loss, con_loss, con_acc, con_log, cross_entropy,
                   weighted_rot_loss, mae, weighted_rec_loss, weighted_con_loss,
                   imgs_recon, r_w, cn_w, rec_w, rot_label, rot_logits]
        return ret_arr

    # noinspection PyProtectedMember
    @tf.function
    def distributed_train_step(dist_inputs):
        _arr = strategy.run(train_step, args=(dist_inputs,))
        rl, loss, cl, ca, loc, rol, wrol, recl, wrecl, wcl, xrec, r_w, cn_w, rec_w, rot_l, rot_lo = _arr

        # Images
        if strategy.num_replicas_in_sync > 1:
            xrec = tf.concat(xrec.values, 0)

        # Losses
        rl = strategy.reduce("SUM", rl, axis=None)
        loss = strategy.reduce("SUM", loss, axis=None)
        cl = strategy.reduce("SUM", cl, axis=None)
        rol = strategy.reduce("SUM", rol, axis=None)
        wrol = strategy.reduce("SUM", wrol, axis=None)
        recl = strategy.reduce("SUM", recl, axis=None)
        wrecl = strategy.reduce("SUM", wrecl, axis=None)
        wcl = strategy.reduce("SUM", wcl, axis=None)

        # Metrics
        if strategy.num_replicas_in_sync > 1:
            ca = tf.concat(ca.values, 0)
            loc = tf.concat(loc.values, 0)
            rot_l = tf.concat(rot_l.values, 0)
            rot_lo = tf.concat(rot_lo.values, 0)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "total_loss__pretrain_train":
                val_.update_state(values=loss)
            if key_ == "regularization_loss__pretrain_train":
                val_.update_state(values=rl)
            if key_ == "contrast_loss__pretrain_train":
                val_.update_state(values=cl)
            if key_ == "weighted_contrast_loss__pretrain_train":
                val_.update_state(values=wcl)
            if key_ == "reconstruction_loss__pretrain_train":
                val_.update_state(values=recl)
            if key_ == "weighted_reconstruction_loss__pretrain_train":
                val_.update_state(values=wrecl)
            if key_ == "rotational_loss__pretrain_train":
                val_.update_state(values=rol)
            if key_ == "weighted_rotational_loss__pretrain_train":
                val_.update_state(values=wrol)
            if key_ == "rotational_weight__pretrain_train":
                val_.update_state(values=r_w)
            if key_ == "contrastive_weight__pretrain_train":
                val_.update_state(values=cn_w)
            if key_ == "reconstructive_weight__pretrain_train":
                val_.update_state(values=rec_w)
            if key_ == "rotational_accuracy__pretrain_train":
                val_.update_state(y_true=tf.argmax(rot_l, -1), y_pred=tf.argmax(rot_lo, -1))
            if key_ == "contrast_accuracy__pretrain_train":
                ca = tf.reduce_mean(tf.cast(ca, tf.float32))
                val_.update_state(ca)
            if key_ == "contrast_entropy__pretrain_train":
                prob_con = tf.nn.softmax(loc)
                entropy_con = -tf.reduce_mean(
                    tf.reduce_sum(prob_con * tf.math.log(prob_con + 1e-8), -1))
                val_.update_state(entropy_con)

        return xrec, (rot_l, rot_lo)

    # noinspection PyProtectedMember
    @tf.function
    def distributed_eval_step(dist_inputs):
        _arr = strategy.run(eval_step, args=(dist_inputs,))
        _, loss, cl, ca, loc, rol, wrol, recl, wrecl, wcl, xrec, r_w, cn_w, rec_w, rot_l, rot_lo = _arr

        # Images
        if strategy.num_replicas_in_sync > 1:
            xrec = tf.concat(xrec.values, 0)

        # Losses
        loss = strategy.reduce("SUM", loss, axis=None)
        cl = strategy.reduce("SUM", cl, axis=None)
        rol = strategy.reduce("SUM", rol, axis=None)
        wrol = strategy.reduce("SUM", wrol, axis=None)
        recl = strategy.reduce("SUM", recl, axis=None)
        wrecl = strategy.reduce("SUM", wrecl, axis=None)
        wcl = strategy.reduce("SUM", wcl, axis=None)

        # Metrics
        if strategy.num_replicas_in_sync > 1:
            ca = tf.concat(ca.values, 0)
            loc = tf.concat(loc.values, 0)
            rot_l = tf.concat(rot_l.values, 0)
            rot_lo = tf.concat(rot_lo.values, 0)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "total_loss__pretrain_eval":
                val_.update_state(values=loss)
            if key_ == "contrast_loss__pretrain_eval":
                val_.update_state(values=cl)
            if key_ == "weighted_contrast_loss__pretrain_eval":
                val_.update_state(values=wcl)
            if key_ == "reconstruction_loss__pretrain_eval":
                val_.update_state(values=recl)
            if key_ == "weighted_reconstruction_loss__pretrain_eval":
                val_.update_state(values=wrecl)
            if key_ == "rotational_loss__pretrain_eval":
                val_.update_state(values=rol)
            if key_ == "weighted_rotational_loss__pretrain_eval":
                val_.update_state(values=wrol)
            if key_ == "rotational_accuracy__pretrain_eval":
                val_.update_state(y_true=tf.argmax(rot_l, -1), y_pred=tf.argmax(rot_lo, -1))
            if key_ == "contrast_accuracy__pretrain_eval":
                ca = tf.reduce_mean(tf.cast(ca, tf.float32))
                val_.update_state(ca)
            if key_ == "contrast_entropy__pretrain_eval":
                prob_con = tf.nn.softmax(loc)
                entropy_con = -tf.reduce_mean(
                    tf.reduce_sum(prob_con * tf.math.log(prob_con + 1e-8), -1))
                val_.update_state(entropy_con)

        return xrec, (rot_l, rot_lo)

    if args.fp16:
        # we use mixed FP16 precision
        # With this policy, layers use float16 computations and float32 variables.
        # Computations are done in float16 for performance, but variables must be kept in
        # float32 for numeric stability.
        policy = tfk.mixed_precision.Policy('mixed_float16')
        tfk.mixed_precision.set_global_policy(policy)

    # Set up Multi-GPU
    strategy = tf.distribute.MirroredStrategy(devices=args.gpus)

    # Get Data
    (test, train), info, train_examples, test_examples = nn.get_data(args.dataset, args=args)
    test = strategy.experimental_distribute_dataset(test)
    train = strategy.experimental_distribute_dataset(train)

    args.per_device_batch_size = int(args.batch_size / strategy.num_replicas_in_sync)

    # Save the full config:
    with open(os.path.join(args.paths.log_dir, "config.txt"), 'w') as f:
        json.dump(nn.without_keys(args.__dict__, ["logger"]), f, indent=2)

    # Build metrics
    metrics = nn.get_metrics(pretrain=True)

    # Get model
    with strategy.scope():
        model, train_info = nn.get_model(args, info)
        model.build(input_shape=(None, args.img_size, args.img_size, args.img_channel))
        # Needed for correctly displaying the Output shapes in summary
        # see: https://stackoverflow.com/a/65956968/4550763
        model.call(tfkl.Input(shape=(args.img_size, args.img_size, args.img_channel)))
        # Print out model summary:
        model.summary(print_fn=args.logger.info)
        # Setting up tensorboard
        summary_writer = tf.summary.create_file_writer(args.paths.log_dir)

        # Define optimizer and learning rate schedule
        warm_up = int(5 * train_info.steps_per_epoch)  # Warmup for 5 epochs
        if args.lr == 0.:
            init_lr = 5e-4 * args.batch_size / 512  # See Table 9 in arXiv:2012.12877 [cs.CV]
        else:
            init_lr = args.lr
        # Set the cosine learning rate scheduler, see arXiv:1812.01187 [cs.CV]
        learning_rate = nn.LinearCosineSchedule(initial_learning_rate=init_lr,
                                                warmup_steps=warm_up,
                                                total_steps=train_info.total_steps)

        # Optimizer
        if args.optimizer.lower() == "sgd":
            optimizer = tfk.optimizers.SGD(learning_rate=learning_rate,
                                           momentum=.9,
                                           nesterov=False)
        elif args.optimizer.lower() == "adam":
            optimizer = tfk.optimizers.Adam(learning_rate=learning_rate,
                                            beta_1=0.9, beta_2=0.999)
        elif args.optimizer.lower() == "adamw":
            # We use the same scheduler for the weight_decay as for the learning_rate
            optimizer = nn.AdamWeightDecay(learning_rate=learning_rate,
                                           weight_decay_rate=args.l2,
                                           beta_1=0.9, beta_2=0.999)
        else:
            raise LookupError("Optimizer should either be 'sgd', 'adamw' or 'adam'")

        if args.fp16:
            # use a dynamic loss scale, see https://www.tensorflow.org/guide/mixed_precision
            optimizer = tfk.mixed_precision.LossScaleOptimizer(optimizer, dynamic=True,
                                                               initial_scale=1 if args.sam else None)

    # start training
    step = 0
    for epoch in range(args.epochs):
        # Eval first
        if epoch % args.test_every_n_epoch == 0:
            it_test = tqdm(test, total=train_info.eval_steps_per_epoch)
            for features in it_test:
                eval_images, eval_labels = features[0], features[1]
                eval_rec_images, eval_rot_pred_image_tuple = distributed_eval_step([eval_images, eval_labels])

                args_tmp = (epoch + 1, args.epochs,
                            metrics.total_loss__pretrain_eval.result().numpy(),
                            metrics.contrast_accuracy__pretrain_eval.result().numpy(),
                            metrics.rotational_accuracy__pretrain_eval.result().numpy())
                st = "Eval || Epoch: {}/{}, Loss: {:.5f}, Contrast Accuracy: {:.5f}, " \
                     "Rotational Accuracy: {:.5f}".format(*args_tmp)
                it_test.set_postfix_str(st)
            args.logger.info(st)

            if summary_writer is not None:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        eval_images = tf.concat(eval_images.values, 0)

                    [eval_undistorted_images, eval_distorted_images] = tf.split(
                        eval_images, num_or_size_splits=2, axis=-1)

                    eval_distorted_list = tf.split(
                        eval_distorted_images, num_or_size_splits=2, axis=-1)
                    eval_distorted_images = tf.concat(eval_distorted_list, 0)

                    eval_undistorted_list = tf.split(
                        eval_undistorted_images, num_or_size_splits=2, axis=-1)
                    eval_undistorted_images = tf.concat(eval_undistorted_list, 0)

                    eval_rot_lbl, eval_rot_log = (eval_rot_pred_image_tuple[0],
                                                  eval_rot_pred_image_tuple[1])
                    eval_rot_preds = tf.nn.softmax(eval_rot_log)
                    eval_rot_preds_binary = tf.one_hot(tf.argmax(eval_rot_preds, -1), 4)
                    eval_rot_pred_image = nn.build_lbl_pred_png(eval_rot_lbl,
                                                                eval_rot_preds_binary,
                                                                eval_rot_preds)

                    tf.summary.image("Distorded Test Images",
                                     nn.normalize_img(eval_distorted_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Undistorded Test Images",
                                     nn.normalize_img(eval_undistorted_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Reconstructed Test Images",
                                     nn.normalize_img(eval_rec_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Prediction Image Rotation Eval",
                                     nn.normalize_img(eval_rot_pred_image, use_image_normalization=False)[0],
                                     step=step)

                    tf.summary.histogram("Test/Distorded Images",
                                         tf.reshape(eval_distorted_images, [-1]),
                                         step=step)
                    tf.summary.histogram("Test/Undistorded Images",
                                         tf.reshape(eval_undistorted_images, [-1]),
                                         step=step)
                    tf.summary.histogram("Test/Reconstructed Images",
                                         tf.reshape(eval_rec_images, [-1]),
                                         step=step)
                    for k, v in zip(metrics._fields, metrics):
                        if "eval" in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=step)

        it_train = tqdm(train, total=train_info.steps_per_epoch)
        for features in it_train:
            step += 1
            # images have 4*channels as two samples are augmented per step and
            # both samples are distorted individually. They are then stacked
            # along the channels dim
            images, labels = features[0], features[1]
            rec_images, rot_pred_image_tuple = distributed_train_step([images, labels])

            if step % args.log_steps == 0 or step == 1:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        images = tf.concat(images.values, 0)

                    [undistorted_images, distorted_images] = tf.split(
                        images, num_or_size_splits=2, axis=-1)

                    distorted_list = tf.split(
                        distorted_images, num_or_size_splits=2, axis=-1)
                    distorted_images = tf.concat(distorted_list, 0)

                    undistorted_list = tf.split(
                        undistorted_images, num_or_size_splits=2, axis=-1)
                    undistorted_images = tf.concat(undistorted_list, 0)

                    rot_lbl, rot_log = rot_pred_image_tuple[0], rot_pred_image_tuple[1]
                    rot_preds = tf.nn.softmax(rot_log)
                    rot_preds_binary = tf.one_hot(tf.argmax(rot_preds, -1), 4)
                    rot_pred_image = nn.build_lbl_pred_png(rot_lbl, rot_preds_binary, rot_preds)

                    tf.summary.image("Distorded Training Images",
                                     nn.normalize_img(distorted_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Undistorded Training Images",
                                     nn.normalize_img(undistorted_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Reconstructed Training Images",
                                     nn.normalize_img(rec_images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Prediction Image Rotation Train",
                                     nn.normalize_img(rot_pred_image, use_image_normalization=False)[0],
                                     step=step)

                    tf.summary.histogram("Train/Distorded Images",
                                         tf.reshape(distorted_images, [-1]),
                                         step=step)
                    tf.summary.histogram("Train/Undistorded Images",
                                         tf.reshape(undistorted_images, [-1]),
                                         step=step)
                    tf.summary.histogram("Train/Reconstructed Images",
                                         tf.reshape(rec_images, [-1]),
                                         step=step)

                    tf.summary.scalar("learning_rate", learning_rate(float(step)), step=step)
                    for k, v in zip(metrics._fields, metrics):
                        if "_train" in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=step)

            args_tmp = (epoch + 1, args.epochs, step, train_info.total_steps,
                        metrics.total_loss__pretrain_train.result().numpy(),
                        metrics.contrast_accuracy__pretrain_train.result().numpy(),
                        metrics.rotational_accuracy__pretrain_train.result().numpy())
            st = "Train || Epoch: {}/{}, Step: {}/{}, Loss: {:.5f}, Contrast Accuracy: {:.5f}, " \
                 "Rotational Accuracy: {:.5f}".format(*args_tmp)
            it_train.set_postfix_str(st)
        args.logger.info(st)

        for m in metrics:
            m.reset_states()

        if epoch % args.save_every_n_epoch == 0:
            model.save_weights(filepath=os.path.join(args.paths.weight_dir, "pretrain_epoch-{}".format(epoch)),
                               save_format="tf")
    # save model after training is completed
    save_str = "pretrain_model_final_after_epoch-{}".format(args.epochs)
    model.save_weights(filepath=os.path.join(args.paths.weight_dir, save_str), save_format="tf")


def main(args):
    # parse arguments
    if args is None:
        exit()

    # Build the model train it
    pre_train(args)


if __name__ == "__main__":
    arguments = nn.get_args()
    arguments.pretrain = True

    # Set up all paths
    arguments.paths = nn.set_path_env(arguments)

    # For logging:
    arguments.logger = tf.get_logger()
    arguments.logger.setLevel(logging.INFO)
    arguments.logger.propagate = False

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(arguments.paths.log_dir, "train.log"))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    arguments.logger.addHandler(fh)

    if not arguments.gpus:
        arguments.logger.info("A supported GPU is necessary")
        exit()
    else:
        tf.config.set_soft_device_placement(True)
        physical_devices = tf.config.list_physical_devices('GPU')
        tf.config.threading.set_inter_op_parallelism_threads(2)
        tf.config.threading.set_intra_op_parallelism_threads(os.cpu_count())
        # noinspection PyBroadException
        try:
            for i, g in enumerate(physical_devices):
                if i < 4:  # GPU with index 4 is the student card on our machine
                    tf.config.experimental.set_memory_growth(g, True)
        except:
            # Invalid device or cannot modify virtual devices once initialized.
            pass
        main(arguments)

        # close the handlers
        for handler in arguments.logger.handlers:
            handler.close()
            arguments.logger.removeFilter(handler)
